#include <TFile.h>
#include <TH2.h>
#include <TStopwatch.h>
#include "Histogram.hh"
#include "HnSparse.hh"
#include "ndhep.hh"

/// \cond CLASSIMP
ClassImp(NDHep::Histogram);
/// \endcond

namespace NDHep {

Histogram::Histogram() : TObject()
{
    ///
    /// A constructor
    ///
}

Histogram::~Histogram()
{
    ///
    /// A destructor
    ///
}

void Histogram::Print(Option_t * /*option*/) const
{
    ///
    /// Printing track info
    ///

    spdlog::info("NDHep::Histogram::Print");
}

void Histogram::Clear(Option_t * /*option*/)
{
    ///
    /// Reseting track to default values
    ///
}
void Histogram::Stress(Long64_t size, bool bytes, std::string filename, bool projections)
{
    ///
    /// Stressing histogram
    ///

    int      nDim      = 10;
    int      nBins     = 1000;
    double   min       = -nBins / 2;
    double   max       = nBins / 2;
    Long64_t chunkSize = 1024 * 1024;

    Int_t    bins[nDim];
    Double_t mins[nDim];
    Double_t maxs[nDim];
    for (Int_t i = 0; i < nDim; i++) {
        bins[i] = nBins;
        mins[i] = min;
        maxs[i] = max;
    }

    HnSparseD * h = new HnSparseD("hNStress", "hNStress", nDim, bins, mins, maxs, chunkSize);
    // h->Stress(2*1e6,10*1024*1024);
    TStopwatch timeStress;
    timeStress.Start();
    h->Stress(size, bytes);
    timeStress.Stop();
    timeStress.Print("m");
    h->Print();
    Long64_t nBinsSizeBytes = sizeof(Double_t) * h->GetNbins();
    spdlog::info("size : {:03.2f} MB ({} B)", (double)nBinsSizeBytes / (1024 * 1024), nBinsSizeBytes);

    TStopwatch timeWrite;
    timeWrite.Start();
    TFile * f = TFile::Open(filename.data(), "RECREATE");
    f->SetCompressionSettings(ROOT::RCompressionSetting::EDefaults::kUseAnalysis);
    spdlog::info("Writing histogram ...");
    h->Write();
    if (projections) {
        spdlog::info("Creating and writing projections ...");
        for (Int_t i = 0; i < nDim; i++) {
            ((TH1 *)h->Projection(i))->Write();
            for (Int_t j = 0; j < nDim; j++) {
                if (i == j) continue;
                ((TH2 *)h->Projection(j, i))->Write();
            }
        }
    }
    f->Close();
    timeWrite.Stop();
    timeWrite.Print("m");
    spdlog::info("Output was saved in '{}'", filename);
}
} // namespace NDHep