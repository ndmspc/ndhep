#pragma once

#include <TObject.h>
#include "HnSparse.hh"
namespace NDHep {

///
/// \class Histogram
///
/// \brief Histogram object
///	\author Martin Vala <mvala@cern.ch>
///

class Histogram : public TObject {

public:
    Histogram();
    virtual ~Histogram();

    virtual void Print(Option_t * option = "") const;
    virtual void Clear(Option_t * option = "");

    virtual void Stress(Long64_t size = 1e3, bool bytes = false, std::string filename = "/tmp/HnSparse.root",bool projections = false);

private:
    /// Copy constructor
    Histogram(const Histogram &);             /// not implemented
    Histogram & operator=(const Histogram &); /// not implemented

    /// \cond CLASSIMP
    ClassDef(Histogram, 1);
    /// \endcond
};
} // namespace NDHep