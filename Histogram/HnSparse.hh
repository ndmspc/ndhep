#pragma once
#ifndef HNSPARSE_H
#define HNSPARSE_H

#include <bits/stdc++.h>
#include <TObject.h>
#include <TStopwatch.h>
#include <THnSparse.h>

namespace NDHep {

///
/// \class HnSparse
///
/// \brief HnSparse object
///	\author Martin Vala <mvala@cern.ch>
///

class HnSparse : public THnSparse {

protected:
    HnSparse();
    HnSparse(const char * name, const char * title, Int_t dim, const Int_t * nbins, const Double_t * xmin = 0,
             const Double_t * xmax = 0, Int_t chunksize = 1024 * 16);

private:
    HnSparse(const HnSparse &);             // Not implemented
    HnSparse & operator=(const HnSparse &); // Not implemented

    bool RecursiveLoop(int & iDim, int * coord);

public:
    virtual void Stress(Long64_t size = 1e3, bool bytes = false);

private:
    Long64_t   fNBytesMax{0}; /// Max size in bytes
    TStopwatch fTimer;        /// Process timer
    TStopwatch fTimerTotal;   /// Total timer

    /// \cond CLASSIMP
    ClassDef(HnSparse, 1);
    /// \endcond
};

//______________________________________________________________________________
/** \class HnSparseT
 Templated implementation of the abstract base THnSparse.
 All functionality and the interfaces to be used are in THnSparse!

 THnSparse does not know how to store any bin content itself. Instead, this
 is delegated to the derived, templated class: the template parameter decides
 what the format for the bin content is. In fact it even defines the array
 itself; possible implementations probably derive from TArray.

 Typedefs exist for template parameters with ROOT's generic types:

 Templated name      |    Typedef   |    Bin content type
 --------------------|--------------|--------------------
 HnSparseT<TArrayC> |  THnSparseC  |  Char_t
 HnSparseT<TArrayS> |  THnSparseS  |  Short_t
 HnSparseT<TArrayI> |  THnSparseI  |  Int_t
 HnSparseT<TArrayL> |  THnSparseL  |  Long_t
 HnSparseT<TArrayF> |  THnSparseF  |  Float_t
 HnSparseT<TArrayD> |  THnSparseD  |  Double_t

 We recommend to use THnSparseC wherever possible, and to map its value space
 of 256 possible values to e.g. float values outside the class. This saves an
 enormous amount of memory. Only if more than 256 values need to be
 distinguished should e.g. THnSparseS or even THnSparseF be chosen.

 Implementation detail: the derived, templated class is kept extremely small
 on purpose. That way the (templated thus inlined) uses of this class will
 only create a small amount of machine code, in contrast to e.g. STL.
*/

template <class CONT>
class HnSparseT : public HnSparse {
public:
    HnSparseT() {}
    HnSparseT(const char * name, const char * title, Int_t dim, const Int_t * nbins, const Double_t * xmin = 0,
              const Double_t * xmax = 0, Int_t chunksize = 1024 * 16)
        : HnSparse(name, title, dim, nbins, xmin, xmax, chunksize)
    {
    }

    TArray * GenerateArray() const { return new CONT(GetChunkSize()); }

private:
    ClassDef(HnSparseT, 1); // Sparse n-dimensional histogram with templated content
};

typedef HnSparseT<TArrayD> HnSparseD;
typedef HnSparseT<TArrayF> HnSparseF;
typedef HnSparseT<TArrayL> HnSparseL;
typedef HnSparseT<TArrayI> HnSparseI;
typedef HnSparseT<TArrayS> HnSparseS;
typedef HnSparseT<TArrayC> HnSparseC;

} // namespace NDHep

#endif /* GRANDFATHER_H */