#include <TAxis.h>
#include <THnSparse.h>

#include "HnSparse.hh"
#include "ndhep.hh"

/// \cond CLASSIMP
ClassImp(NDHep::HnSparse);
/// \endcond

namespace NDHep {

HnSparse::HnSparse() : THnSparse() {}

HnSparse::HnSparse(const char * name, const char * title, Int_t dim, const Int_t * nbins, const Double_t * xmin,
                   const Double_t * xmax, Int_t chunksize)
    : THnSparse(name, title, dim, nbins, xmin, xmax, chunksize)
{
    ///
    /// A constructor
    ///
}

bool HnSparse::RecursiveLoop(int & iDim, int * coord)
{
    ///
    /// Recursive loop
    ///

    Long64_t nBinsSizeBytes = sizeof(Double_t) * GetNbins();
    if (fNBytesMax > 0 && nBinsSizeBytes > fNBytesMax) return true;

    if (iDim >= fNdimensions) return true;
    if (coord[fNdimensions - 1] > GetAxis(fNdimensions - 1)->GetNbins()) {
        return true;
    }

    if (nBinsSizeBytes > 0 && nBinsSizeBytes % (10 * 1024 * 1024) == 0) {
        spdlog::debug("{:03.2f} MB [chunks={} binsFilled={}]", (double)nBinsSizeBytes / (1024 * 1024), GetNChunks(),
                      GetNbins());
        fTimer.Stop();
        fTimer.Print("m");
        fTimer.Reset();
        fTimer.Start();
    }

    spdlog::trace("iDim={} nBins={}", iDim, GetAxis(iDim)->GetNbins());
    if (coord[iDim] < GetAxis(iDim)->GetNbins()) {
        coord[iDim]++;
        coord[iDim - 1] = 0;
        iDim            = 0;
        return false;
    }

    return RecursiveLoop(++iDim, coord);
}
void HnSparse::Stress(Long64_t size, bool bytes)
{
    ///
    /// Stress function
    ///
    Long64_t nFill = size;
    if (bytes) {
        fNBytesMax = size;
        nFill      = kMaxLong64;
    }
    spdlog::info("dimensions={} chunkSize={} nFill={} maxSize={}", GetNdimensions(), GetChunkSize(), nFill, fNBytesMax);


    int    c[fNdimensions];
    double cCenter[fNdimensions];
    for (int i = 0; i < fNdimensions; ++i) {
        c[i] = 100;
    }

    fTimerTotal.Start();
    fTimer.Start();
    bool finish = false;
    int  dim;
    for (int iFill = 0; iFill < nFill; ++iFill) {
        spdlog::trace("iFill={}", iFill);
        dim    = 0;
        finish = RecursiveLoop(dim, c);
        if (finish) return;

        std::string s = "[";
        for (int i = 0; i < fNdimensions; ++i) {
            spdlog::trace("coord[{}]={}", i, c[i]);

            cCenter[i] = GetAxis(i)->GetBinCenter(c[i]);
            s.append(fmt::format("{},", cCenter[i]));
        }
        s.resize(s.size() - 1);
        s.append("]");
        spdlog::trace("coord={}", s);
        // Fill(cCenter);
        GetBin(cCenter);
    }
    fTimer.Stop();
    fTimerTotal.Stop();
    fTimerTotal.Print("m");
}

} // namespace NDHep