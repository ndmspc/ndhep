#if defined(__CINT__) || defined(__ROOTCLING__)

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class NDHep::HnSparse + ;
#pragma link C++ class NDHep::HnSparseT<TArrayD> + ;
#pragma link C++ class NDHep::HnSparseT<TArrayF> + ;
#pragma link C++ class NDHep::HnSparseT<TArrayL> + ;
#pragma link C++ class NDHep::HnSparseT<TArrayI> + ;
#pragma link C++ class NDHep::HnSparseT<TArrayS> + ;
#pragma link C++ class NDHep::HnSparseT<TArrayC> + ;

#pragma link C++ class NDHep::Histogram + ;
#endif
