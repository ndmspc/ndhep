# Find UUID
#
# Find the UUID includes and library
# 
# if you nee to add a custom library search path, do it via via CMAKE_PREFIX_PATH 
# 
# This module defines
#  UUID_INCLUDE_DIRS, where to find header, etc.
#  UUID_LIBRARIES, the libraries needed to use UUID.
#  UUID_FOUND, If false, do not try to use UUID.
#  UUID_INCLUDE_PREFIX, include prefix for UUID

# only look in default directories
find_path(
	UUID_INCLUDE_DIR 
	NAMES uuid.h
	DOC "UUID include dir"
	HINTS /usr/include/uuid
)

find_library(
	UUID_LIBRARY
	NAMES uuid
	DOC "UUID library"
)

set(UUID_INCLUDE_DIRS ${UUID_INCLUDE_DIR})
set(UUID_LIBRARIES ${UUID_LIBRARY})

# handle the QUIETLY and REQUIRED arguments and set UUID_FOUND to TRUE
# if all listed variables are TRUE, hide their existence from configuration view
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(UUID DEFAULT_MSG UUID_LIBRARY UUID_INCLUDE_DIR)
mark_as_advanced (UUID_FOUND UUID_INCLUDE_DIR UUID_LIBRARY)
