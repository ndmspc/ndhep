# Find the SPDLOG include directory
# The following variables are set if SPDLOG is found.
#  SPDLOG_FOUND        - True when the SPDLOG include directory is found.
#  SPDLOG_INCLUDE_DIR  - The path to where the SPDLOG include files are.
# If SPDLOG is not found, SPDLOG_FOUND is set to false.

find_path (
        SPDLOG_INCLUDE_DIRS
        NAMES spdlog/spdlog.h
)

if(NOT EXISTS "${SPDLOG_INCLUDE_DIRS}/spdlog/fmt/bundled/core.h")
  option(SPDLOG_FMT_EXTERNAL "Enable SPDLOG_FMT_EXTERNAL" ON)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args (SPDLOG DEFAULT_MSG SPDLOG_INCLUDE_DIRS)
mark_as_advanced (SPDLOG_FOUND SPDLOG_INCLUDE_DIRS SPDLOG_FMT_EXTERNAL)
