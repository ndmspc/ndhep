# Find DAOS
#
# Find the DAOS includes and library
# 
# if you nee to add a custom library search path, do it via via CMAKE_PREFIX_PATH 
# 
# This module defines
#  DAOS_INCLUDE_DIRS, where to find header, etc.
#  DAOS_LIBRARIES, the libraries needed to use DAOS.
#  DAOS_FOUND, If false, do not try to use DAOS.
#  DAOS_INCLUDE_PREFIX, include prefix for DAOS

# only look in default directories
find_path(
	DAOS_INCLUDE_DIR 
	NAMES daos.h
	DOC "DAOS include dir"
	HINTS /usr/include/DAOS-cpp
)

find_library(
	DAOS_LIBRARY
	NAMES daos
	DOC "DAOS library"
)

set(DAOS_INCLUDE_DIRS ${DAOS_INCLUDE_DIR})
set(DAOS_LIBRARIES ${DAOS_LIBRARY})

# handle the QUIETLY and REQUIRED arguments and set DAOS_FOUND to TRUE
# if all listed variables are TRUE, hide their existence from configuration view
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(DAOS DEFAULT_MSG DAOS_LIBRARY DAOS_INCLUDE_DIR)
mark_as_advanced (DAOS_FOUND DAOS_INCLUDE_DIR DAOS_LIBRARY)
