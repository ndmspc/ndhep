#include <fstream>
#include <iostream>
#include <bits/stdc++.h>
#include <TString.h>
#include <TRandom.h>
#include "GitlabEvent.hh"

/// \cond CLASSIMP
ClassImp(NDHep::Gitlab::Event);
/// \endcond

namespace NDHep {
namespace Gitlab {

Event::Event() : TObject(), fID(0), fNIssues(0), fNMergeRequests(0), fIssues(0), fMergeRequests(0)
{
    ///
    /// Default constructor
    ///
}

Event::Event(Long64_t id) : TObject(), fID(id), fNIssues(0), fNMergeRequests(0), fIssues(0), fMergeRequests(0)
{
    ///
    /// A constructor
    ///
    fIssues        = new TClonesArray("NDHep::Gitlab::Track");
    fMergeRequests = new TClonesArray("NDHep::Gitlab::Track");
    gRandom->SetSeed(0);
}

Event::~Event()
{
    ///
    /// A destructor
    ///

    delete fIssues;
    fIssues = 0;
    delete fMergeRequests;
    fMergeRequests = 0;
}

Track * Event::AddIssue()
{
    ///
    /// Adds Issue to event
    ///
    return (Track *)fIssues->ConstructedAt(fNIssues++);
}

Track * Event::AddMergeRequest()
{
    ///
    /// Adds Merge requests to event
    ///
    return (Track *)fMergeRequests->ConstructedAt(fNMergeRequests++);
}

bool Event::FillGitlabFromJson(std::string issues, std::string mergrerequests)
{
    ///
    /// Import gitlab info from json input
    ///

    Json::CharReaderBuilder builder;
    builder["collectComments"] = false;
    Json::Value iss, mrs;
    // Json::String errs;
    std::string errs;

    if (!issues.empty()) {
        Printf("Processing '%s' ...", issues.data());
        std::ifstream fileIssues(issues.data());
        if (!fileIssues.is_open()) {
            Printf("Error: Unable to open file '%s' !!!", issues.data());
            return false;
        }

        if (!parseFromStream(builder, fileIssues, &iss, &errs)) {
            Printf("Error: Failed to parse file '%s' !!!", issues.data());
            Printf("%s", errs.data());
            return false;
        }
        FillIssuesFromJson(iss);
    }
    if (!mergrerequests.empty()) {
        Printf("Processing '%s' ...", mergrerequests.data());
        std::ifstream fileMRs(mergrerequests);
        if (!fileMRs.is_open()) {
            Printf("Error: Unable to open file '%s' !!!", mergrerequests.data());
            return false;
        }

        if (!parseFromStream(builder, fileMRs, &mrs, &errs)) {
            Printf("Error: Failed to parse file '%s' !!!", mergrerequests.data());
            Printf("%s", errs.data());
            return false;
        }
        FillMergeRequestsFromJson(mrs);
    }
    return true;
}
bool Event::FillIssuesFromJson(const Json::Value root)
{
    ///
    /// Import gitlab issues info from json input
    ///

    for (const auto & jv : root) {
        Track * t = AddIssue();
        t->SetState(jv["state"].asString());
        t->SetProjectID(jv["project_id"].asInt());
        t->SetAuthorID(jv["author"]["id"].asInt());
        t->SetProject(ParseProjectName(jv["references"]["full"].asString(), '#'));
        t->SetAuthor(jv["author"]["username"].asString());
        FillAuthorProjectAxis(t->GetAuthor(), t->GetProject());
        Printf("Issue %d project [%s] author [%s]", jv["iid"].asInt(), t->GetProject().data(), t->GetAuthor().data());
    }

    return true;
}
bool Event::FillMergeRequestsFromJson(const Json::Value root)
{
    ///
    /// Import gitlab merge requests info from json input
    ///

    for (const auto & jv : root) {
        Track * t = AddMergeRequest();
        t->SetState(jv["state"].asString());
        t->SetProjectID(jv["project_id"].asInt());
        t->SetAuthorID(jv["author"]["id"].asInt());
        t->SetProject(ParseProjectName(jv["references"]["full"].asString(), '!'));
        t->SetAuthor(jv["author"]["username"].asString());

        FillAuthorProjectAxis(t->GetAuthor(), t->GetProject());
        Printf("MR %d project [%s] author [%s]", jv["iid"].asInt(), t->GetProject().data(), t->GetAuthor().data());
    }

    return true;
}

void Event::Print(Option_t * option) const
{
    ///
    /// Printing event info
    ///
    Printf("id=%lld nIssues=%d nMergeRequests=%d", fID, fNIssues, fNMergeRequests);

    // if (!fTracks) return;

    // TString str(option);
    // str.ToLower();
    // if (str.Contains("all")) {
    //     Track * t;
    //     for (Int_t i = 0; i < fTracks->GetEntries(); i++) {
    //         t = (Track *)fTracks->At(i);
    //         t->Print();
    //     }
    // }
}

void Event::Clear(Option_t *)
{
    ///
    /// Reseting event to default values and clear all tracks
    ///
    fID = 0;
    // fDateTime.Reset();

    fNIssues = 0;
    fIssues->Clear("C");
    fNMergeRequests = 0;
    fMergeRequests->Clear("C");
}

void Event::SetTimeDate(Int_t year, Int_t month, Int_t day, Int_t hour, Int_t min, Int_t sec)
{
    ///
    /// Sets Date and time of event
    ///
    fDateTime.Set(year, month, day, hour, min, sec);
}

std::string Event::ParseProjectName(std::string in, char d) const
{
    ///
    /// Parse project name
    ///
    std::string       s;
    std::stringstream ss(in);
    std::getline(ss, s, d);

    return std::move(s);
}

void Event::FillAuthorProjectAxis(std::string author, std::string project)
{
    ///
    /// Fill Author and project axis
    ///
    if (!fAuthors || !fProjects) return;

    Int_t b = fAuthors->FindBin(author.data());
    if (b == -1 && !author.empty()) {
        fAuthors->SetBinLabel(fAuthors->GetNbins(), author.data());
        fAuthors->Set(fAuthors->GetNbins() + 1, 0, fAuthors->GetNbins() + 1);
    }

    b = fProjects->FindBin(project.data());
    if (b == -1 && !project.empty()) {
        fProjects->SetBinLabel(fProjects->GetNbins(), project.data());
        fProjects->Set(fProjects->GetNbins() + 1, 0, fProjects->GetNbins() + 1);
    }
}

} // namespace Gitlab

} // namespace NDHep