#!/bin/bash
export WKDIR="$NDMBASE/$NDMPATH"
mkdir -p $WKDIR && cd $WKDIR || exit 1
(
NDTEST_N=${NDTEST_N-1}
NDTEST_IN_FILE=${NDTEST_IN_FILE-"/tmp/HnSparse.root"}
NDTEST_IN_OBJNAME=${NDTEST_IN_OBJNAME-"hNStress"}
NDTEST_OUT_FILE=${NDTEST_OUT_FILE-"/tmp/HnSparseWrite.root"}
NDTEST_OBJ_PER_FILE=${NDTEST_OBJ_PER_FILE-1}
NDTEST_MON_URL=${NDTEST_MON_URL-">tcp://localhost:15000"}
hostname
ndm info -o point.json
MIN=$(cat point.json | jq '.coordinates[0].min')
echo $MIN
NDTEST_OUT_FILE=${NDTEST_OUT_FILE//.root/_${MIN}.root}
source $NDHEP_DIR/scripts/env.sh
export
root -b -q -l $NDHEP_DIR/macros/test/HnSparse/HnSparseWrite.C'('$NDTEST_N',"'$NDTEST_IN_FILE'","'$NDTEST_IN_OBJNAME'","'$NDTEST_OUT_FILE'",'$NDTEST_OBJ_PER_FILE',"'$NDTEST_MON_URL'")' || exit 1
ERR=$(cat $WKDIR/ndm.log | grep error | wc -l)
[ $ERR -eq 0 ] || exit 1

# my job
#sleep 1
) |& tee $WKDIR/ndm.log