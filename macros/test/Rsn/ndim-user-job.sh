#!/bin/bash
# source ~/.bashrc
# module add ROOT/v6-18-00 ndm-lite/v1.0 # Enable for HybriLIT cluster
RSN_INPUT=${RSN_INPUT-"root://alieos.saske.sk//eos/alike.saske.sk/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/713_20170804-2039/merge_runlist_4/PhiKK_SP_MB_3s/AnalysisResults.root"}
RSN_SPARSE=${RSN_SPARSE-"Unlike,Mixing"}
RSN_PROJ_ID=${RSN_PROJ_ID-"1"}

RSN_PATH=${RSN_PATH-"NDMPOINT"}
RSN_OUTPUT=${RSN_OUTPUT-"root://eos1.hydra.local//eos/hybrilit.jinr.ru/projects/lit/hybrilit/ndm/test"}
RSN_USECACHE=${RSN_USECACHE-"0"}
RSN_OUTPUT_FILE=${RSN_OUTPUT_FILE-"AnalysisResults.root"}
echo -e "$RSN_INPUT\n$RSN_SPARSE\n$RSN_PROJ_ID\n$RSN_PATH\n$RSN_OUTPUT\n$RSN_USECACHE"
time root -b -q $NDMBASE/SplitSparse.C'("'$RSN_INPUT'","'$RSN_SPARSE'","'$RSN_PROJ_ID'",0,0,"'$RSN_PATH'","'$RSN_OUTPUT'",'$RSN_USECACHE')' ||{ rc=$?; echo "Failed SplitSparse.C"; exit $rc; } 
time root -b -q $NDMBASE/Rsn1DProjection.C'("'${RSN_OUTPUT}/${NDMPATH}${RSN_OUTPUT_FILE}'",0,"")' || { rc=$?; echo "Failed Rsn1DProjection.C"; exit $rc; } 
date
export END=$(date +%s.%N)
