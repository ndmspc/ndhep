#include <TString.h>
#include <TFile.h>
#include <THnSparse.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TF1.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>

Double_t Pol2(double * x, double * par)
{
    return par[0] + x[0] * par[1] + x[0] * x[0] * par[2];
}
Double_t VoigtPol2(double * x, double * par)
{
    return par[0] * TMath::Voigt(x[0] - par[1], par[3], par[2]) + Pol2(x, &par[4]);
}
Double_t GetChi2(TFitResultPtr fFitResult)
{
    return fFitResult->Chi2();
}

Double_t GetNdf(TFitResultPtr fFitResult)
{
    return fFitResult->Ndf();
}

Double_t GetReducedChi2(TFitResultPtr fFitResult)
{
    return fFitResult->Chi2() / fFitResult->Ndf();
}

Double_t GetProb(TFitResultPtr fFitResult)
{
    return fFitResult->Prob();
}
TH1 * fillParams(TF1 * sigBgFnc, TH1 * peak, TFitResultPtr fFitResult, Int_t bin_min, Int_t bin_max);

void GetYieldBinCounting(TH1 * h, TFitResultPtr fFitResult, Int_t bin_min, Int_t bin_max, Double_t & val,
                         Double_t & err, Double_t eps = 1e-4);

void GetYieldFitFunction(TH1 * h, TFitResultPtr fFitResult, Int_t bin_min, Int_t bin_max, Double_t & val,
                         Double_t & err, Double_t eps = 1e-4);

int Rsn1DProjection(TString input = "AnalysisResults.root", Int_t idAxis = 0,
                    TString outputFilename = "RsnProjection.root")
{
    TH1::AddDirectory(0);

    TFile * f = TFile::Open(input);
    if (!f) {
        Printf("Error: Cannot open file '%s' !", input.Data());
        return 1;
    }
    THnSparseD * unlike = (THnSparseD *)f->Get("Unlike");
    THnSparseD * mixing = (THnSparseD *)f->Get("Mixing");
    if (!(unlike && mixing)) {
        Printf("Sparses cannot be found. Aborting.");
        return 2;
    }

    TH1 * sigBgProj = (TH1 *)unlike->Projection(idAxis);
    sigBgProj->SetName("sigBg");
    TH1 * bgProj = (TH1 *)mixing->Projection(idAxis);
    bgProj->SetName("bg");
    TH1 * bgProjNorm = (TH1 *)bgProj->Clone();
    bgProjNorm->SetName("bgNorm");
    TH1 * peak = (TH1 *)sigBgProj->Clone();
    peak->SetName("peak");

    Int_t binNormMin = 110;
    Int_t binNormMax = 150;
    // better norm : best for fitting is to have all points above 0(zero)

    // Setting fitting range
    Double_t fitMin = 0.997;
    Double_t fitMax = 1.050;

    Double_t norm = sigBgProj->Integral(binNormMin, binNormMax) / bgProjNorm->Integral(binNormMin, binNormMax);
    bgProjNorm->Scale(norm); // scaling the bgProjNorm histogram
    peak->Add(bgProjNorm, -1);

    // Some initial values for fitting
    const Double_t phi_mass  = 1.019445;
    const Double_t phi_width = 0.00426;
    const Double_t phi_sigma = 0.001;
    TF1 *          sigBgFnc  = new TF1("VoightPol2", VoigtPol2, fitMin, fitMax, 7);
    TF1 *          bgFnc     = new TF1("Pol2", Pol2, fitMin, fitMax, 3);

    Double_t p0p = 0;
    // p0p = peak->Integral(peak->FindBin(fitMin), peak->FindBin(fitMax)) * peak->GetBinWidth(peak->FindBin(fitMin));

    // Sets init parameters for fitting
    sigBgFnc->SetParameters(p0p, phi_mass, phi_width, phi_sigma, 0.0, 0.0, 0.0, 0.0);

    // set parameter names
    sigBgFnc->SetParNames("yield", "mass", "width", "sigma", "p0", "p1", "p2", "p3");

    // Fixing parameter for resolution
    sigBgFnc->FixParameter(3, phi_sigma);

    // Doing fit
    Int_t nFit = 10;
    for (Int_t iFit = 0; iFit < nFit; iFit++) {
        peak->Fit(sigBgFnc, "QN MFC", "", fitMin, fitMax);
    }

    TFitResultPtr fFitResult = peak->Fit(sigBgFnc, "QN MF S", "", fitMin, fitMax);
    Double_t      par[6];
    sigBgFnc->GetParameters(par);
    const Double_t * parErr = sigBgFnc->GetParErrors();

    // Getting pol2 part from fit and set it to bgFnc for drawing and then
    // subtracting
    bgFnc->SetParameters(&par[4]);

    // add fit and background function to histogram so it is automatically drawn
    // with hist
    peak->GetListOfFunctions()->Add(sigBgFnc);
    peak->GetListOfFunctions()->Add(bgFnc);

    Int_t bin_min = peak->GetXaxis()->FindBin(fitMin);
    Int_t bin_max = peak->GetXaxis()->FindBin(fitMax);
    if (TMath::IsNaN(sigBgProj->GetEntries()) || TMath::IsNaN(bgProj->GetEntries()) ||
        TMath::IsNaN(bgProjNorm->GetEntries()) || TMath::IsNaN(peak->GetEntries())) {
        return 0;
    }
    TH1 * resultParams = fillParams(sigBgFnc, peak, fFitResult, bin_min, bin_max);

    if (!gROOT->IsBatch()) {
        TCanvas * c = new TCanvas("rsn", "Rsn Analysis");
        c->Divide(2, 2);
        c->cd(1);
        sigBgProj->DrawCopy();
        c->cd(2);
        bgProj->DrawCopy();
        c->cd(3);
        bgProjNorm->SetLineColor(kGreen);
        bgProjNorm->DrawCopy();
        sigBgProj->DrawCopy("SAME");
        c->cd(4);
        peak->DrawCopy();
    }
    TString output = outputFilename;
    if (outputFilename.IsNull()) {
        output = input;
        output.ReplaceAll("AnalysisResults.root", "RsnProjection.root");
    }
    Printf("Saving output to '%s' ...", output.Data());
    TFile * out = TFile::Open(output.Data(), "RECREATE");
    if (!out) {
        Printf("Error: Cannot open file '%s' !", output.Data());
        return 3;
    }

    sigBgProj->Write();
    bgProj->Write();
    bgProjNorm->Write();
    peak->Write();
    if (resultParams) resultParams->Write();
    out->Close();
    f->Close();

    delete f, out;
    return 0;
}

TH1 * fillParams(TF1 * sigBgFnc, TH1 * peak, TFitResultPtr fFitResult, Int_t bin_min, Int_t bin_max)
{
    Double_t    val, err;
    const Int_t nBins = 6 + sigBgFnc->GetNpar();
    Double_t    par[6];
    Double_t    parErr[6];
    TH1 *       resultParams = new TH1D("resultParams", "Result parameters", nBins, 0, nBins);
    Int_t       iBin         = 1;

    GetYieldBinCounting(peak, fFitResult, bin_min, bin_max, val, err);
    if (TMath::IsNaN(val) || TMath::IsNaN(err)) {
        delete resultParams;
        return nullptr;
    }
    resultParams->SetBinContent(iBin, val);
    resultParams->SetBinError(iBin, err);
    resultParams->GetXaxis()->SetBinLabel(iBin, "IntBC");
    iBin++;

    GetYieldFitFunction(peak, fFitResult, bin_min, bin_max, val, err);
    if (TMath::IsNaN(val) || TMath::IsNaN(err)) {
        delete resultParams;
        return nullptr;
    }
    resultParams->SetBinContent(iBin, val);
    resultParams->SetBinError(iBin, err);
    resultParams->GetXaxis()->SetBinLabel(iBin, "IntFF");
    iBin++;

    resultParams->SetBinContent(iBin, GetChi2(fFitResult));
    resultParams->SetBinError(iBin, 0);
    resultParams->GetXaxis()->SetBinLabel(iBin, "Chi2");
    iBin++;

    resultParams->SetBinContent(iBin, GetNdf(fFitResult));
    resultParams->SetBinError(iBin, 0);
    resultParams->GetXaxis()->SetBinLabel(iBin, "Ndf");
    iBin++;

    resultParams->SetBinContent(iBin, GetReducedChi2(fFitResult));
    resultParams->SetBinError(iBin, 0);
    resultParams->GetXaxis()->SetBinLabel(iBin, "ReducedChi2");
    iBin++;

    resultParams->SetBinContent(iBin, GetProb(fFitResult));
    resultParams->SetBinError(iBin, 0);
    resultParams->GetXaxis()->SetBinLabel(iBin, "Prob");
    iBin++;

    for (Int_t i = 0; i < sigBgFnc->GetNpar(); i++) {
        if (TMath::IsNaN(par[i]) || TMath::IsNaN(parErr[i])) {
            delete resultParams;
            return nullptr;
        }
        resultParams->SetBinContent(iBin, par[i]);
        resultParams->SetBinError(iBin, parErr[i]);
        resultParams->GetXaxis()->SetBinLabel(iBin, sigBgFnc->GetParName(i));
        iBin++;
    }
    return resultParams;
}

void GetYieldBinCounting(TH1 * h, TFitResultPtr fFitResult, Int_t bin_min, Int_t bin_max, Double_t & val,
                         Double_t & err, Double_t eps = 1e-4)
{

    if (!h) return;

    Double_t min = h->GetXaxis()->GetBinLowEdge(bin_min);
    Double_t max = h->GetXaxis()->GetBinUpEdge(bin_max);

    Double_t histWidth = h->GetXaxis()->GetBinWidth(1);

    val = h->IntegralAndError(bin_min, bin_max, err);

    TF1 * bgFnc = (TF1 *)h->GetListOfFunctions()->At(1);

    Double_t bg = bgFnc->Integral(min, max, eps);

    // TODO Verify it
    Double_t bgErr = bgFnc->IntegralError(min, max, fFitResult->GetParams(),
                                          fFitResult->GetCovarianceMatrix().GetMatrixArray(), eps);

    bg /= histWidth;
    bgErr /= histWidth;

    val -= bg;
    err = TMath::Sqrt(TMath::Power(err, 2) + TMath::Power(bgErr, 2));
}

void GetYieldFitFunction(TH1 * h, TFitResultPtr fFitResult, Int_t bin_min, Int_t bin_max, Double_t & val,
                         Double_t & err, Double_t eps = 1e-4)
{

    if (!h) return;

    Double_t min = h->GetXaxis()->GetBinLowEdge(bin_min);
    Double_t max = h->GetXaxis()->GetBinUpEdge(bin_max);

    Double_t histWidth = h->GetXaxis()->GetBinWidth(1);

    TF1 * sigBgFnc = (TF1 *)h->GetListOfFunctions()->At(0);
    TF1 * bgFnc    = (TF1 *)h->GetListOfFunctions()->At(1);

    val = sigBgFnc->Integral(min, max, eps);
    err = sigBgFnc->IntegralError(min, max, fFitResult->GetParams(), fFitResult->GetCovarianceMatrix().GetMatrixArray(),
                                  eps);

    val /= histWidth;
    err /= histWidth;

    Double_t bg = bgFnc->Integral(min, max, eps);

    // TODO Verify it
    Double_t bgErr = bgFnc->IntegralError(min, max, fFitResult->GetParams(),
                                          fFitResult->GetCovarianceMatrix().GetMatrixArray(), eps);

    bg /= histWidth;
    bgErr /= histWidth;

    val -= bg;
    err = TMath::Sqrt(TMath::Power(err, 2) + TMath::Power(bgErr, 2));
}