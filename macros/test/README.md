## Creating ndm-lite for HybriLIT
1. Copy the file [Point.proto](https://gitlab.com/ndmspc/ndm/-/raw/master/proto/Point.proto) to the working directory.
2. Make directories 
```shell
mkdir ndm-lite
cd ndm-lite
mkdir include/ndm lib64
```
3. Compile the `.proto` file via: 
```shell
module add protobuf/v3.11.3
protoc --cpp_out=. Point.proto
```
4. Build library `libndim_proto.so` through: 
```shell
gcc -shared -fPIC Point.pb.cc -std=c++11 -I$PROTOBUF_ROOT/include -L$PROTOBUF_ROOT/lib -lprotobuf -o lib64/libndim_proto.so
```
5. Place header `Point.pb.h` in `include/ndm`. `Point.pb.cc` can be discarded after this step.
```
mv Point.pb.h include/ndm/
```
6. Export environment variables:
```shell
export NDM_LITE="<path to ndm-lite>"
export LD_LIBRARY_PATH="$NDM_LITE/lib64:$LD_LIBRARY_PATH"
export CPLUS_INCLUDE_PATH="$NDM_LITE/include/ndm:$CPLUS_INCLUDE_PATH"
```
7. Finally, the working directory should have 3 members: `Point.proto`, `include/ndm/Point.pb.h` and `lib64/libndim_proto.so`.
