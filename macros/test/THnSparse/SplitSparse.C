#include <string>
#include <TString.h>
#include <ndm/Point.pb.h>
R__LOAD_LIBRARY(libndim_proto)

void asci_to_protobuf(std::string a, std::string & p)
{
    std::stringstream sstream(a);
    p.clear();
    while (sstream.good()) {
        std::bitset<8> bits;
        sstream >> bits;
        char c = char(bits.to_ulong());
        p += c;
    }
}

using namespace std;

int SplitSparse(
    std::string input =
        "root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/cern/alice/rsn/alice/cern.ch/user/a/alitrain/PWGLF/"
        "LF_pp/713_20170804-2039/merge_runlist_4/PhiKK_SP_MB_3s/AnalysisResults.root",
    std::string sparse = "Unlike Mixing", std::string id = "1", double min = 0, double max = 0,
    std::string path   = "NDMPOINT",
    std::string output = "root://eos1.hydra.local//eos/hybrilit.jinr.ru/projects/lit/hybrilit/ndm/test",
    bool useCache = false, bool overwrite = false)
{
    NDM::Point p;

    std::stringstream ss(sparse);
    std::string       item;
    if (path == "NDMPOINT") {
        std::string strasci2;
        asci_to_protobuf(getenv("NDMPOINT"), strasci2);
        Printf("Running from NDMPOINT with coordinates:");
        p.ParseFromString(strasci2);
    }
    else {
        p.set_path(path);
        NDM::Coordinate * coord = p.add_coordinates();
        coord->set_min(min);
        coord->set_max(max);
        coord->set_info(id);
    }
    p.PrintDebugString();
    Printf("Input file '%s' histograms : (%s)", input.data(), sparse.data());

    TH1::AddDirectory(kFALSE);
    if (useCache) {
        TFile::SetCacheFileDir(gSystem->HomeDirectory(), 1, 1);
    }

    TFile * fileInput = TFile::Open(input.data());
    if (!fileInput) {
        Printf("Error: Cannot open file '%s' !", input.data());
        return 1;
    }

    if (output.back() == '/')
        output += p.path() + (gSystem->BaseName(input.data()));
    else
        output += "/" + p.path() + (gSystem->BaseName(input.data()));

    TFile * fileOutput;
    if (!overwrite) {
        fileOutput = TFile::Open(output.data(), "READ");
        if (fileOutput) {
            Printf("File '%s' already exists !!! Skipping ...", output.data());
            return 0;
        }
    }

    fileOutput = TFile::Open(output.data(), "RECREATE");
    if (!fileOutput) {
        Printf("Error: Cannot open file '%s' !", output.data());
        return 3;
    }

    string      title;
    THnSparseD *hsInput, *hsOutput;
    int         binMin, binMax;
    while (std::getline(ss, item, ',')) {
        Printf("Processing histogram named '%s' ...", item.data());

        hsInput = (THnSparseD *)fileInput->Get(item.data());
        if (!hsInput) {
            Printf("Error: Cannot open histogram '%s' !", item.data());
            return 4;
        }
        int dim[hsInput->GetNdimensions()];
        for (int d = 0; d < hsInput->GetNdimensions(); d++) {
            dim[d] = d;
        }
        for (int iCoord = 0; iCoord < p.coordinates_size(); iCoord++) {
            auto c = p.coordinates(iCoord);
            if (c.isbin())
                hsInput->GetAxis(stoi(c.info()))->SetRange(c.min(), c.max() - 1);
            else {
                binMin = hsInput->GetAxis(stoi(c.info()))->FindFixBin(c.min());
                binMax = hsInput->GetAxis(stoi(c.info()))->FindFixBin(c.max());
                hsInput->GetAxis(stoi(c.info()))->SetRange(binMin, binMax);
            }
        }

        hsOutput = (THnSparseD *)hsInput->Projection(hsInput->GetNdimensions(), &dim[0], "O");
        if (!hsOutput) {
            Printf("Error: Problem getting projection from '%s' histogram !!!", item.data());
            return 5;
        }
        title = hsInput->GetTitle();
        if (!title.empty()) title += " ";
        for (int iCoord = 0; iCoord < p.coordinates_size(); iCoord++) {
            auto c = p.coordinates(iCoord);
            title +=
                TString::Format(("%s<%.1f, %.1f) "), hsInput->GetAxis(stoi(c.info()))->GetName(), c.min(), c.max());
            Printf("title: %s", title.data());
        }
        hsOutput->SetNameTitle(hsInput->GetName(), title.data());

        Printf("Writing histogram '%s' with title '%s' to file %s  ...", hsOutput->GetName(), hsOutput->GetTitle(),
               output.data());
        hsOutput->Write();
        delete hsOutput, hsInput;
    }

    fileInput->Close();
    delete fileInput;
    fileOutput->Close();
    delete fileOutput;
    return 0;
}