## How to run on cluster
1. Copy the file ``Point.proto`` to the working directory.

2. Compile it via: 
```shell
protoc --cpp_out=. Point.proto
```
Note: To compile the above command `protoc`, you need to have the `protobuf` module loaded.

3. Build library `libndim_proto.so` through: 
```shell
gcc -shared -fPIC Point.pb.cc -std=c++11 -I$PROTOBUF_ROOT/include -L$PROTOBUF_ROOT/lib -lprotobuf -o libndim_proto.so
```