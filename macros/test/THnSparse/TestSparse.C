#include <TMacro.h>
#include <TString.h>
#include <Axis.hh>
using namespace std;
R__LOAD_LIBRARY(libndim)

int TestSparse(string input = "root://alieos.saske.sk//eos/alike.saske.sk/alice/cern.ch/user/a/alitrain/PWGLF/LF_pp/"
                              "713_20170804-2039/merge_runlist_4/PhiKK_SP_MB_3s/AnalysisResults.root",
               string prefix_out = "root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/scratch/ndmtest/", int IDaxis = 1,
               bool useCache = true, string sparse = "Unlike Mixing")

{

    double         min, max;
    string         path;
    NDM::Axis      a(0, 200, NDM::Axis::kFixed);
    vector<double> values = {0, 50, 100, 150};
    TMacro         userJob("UserJob.C");

    for (auto v : values) {
        path = "";
        a.find(v, min, max, path, 2);
        path += "/";
        TString args = TString::Format("\"%s\",\"%s\",\"%d\",%f,%f,\"%s\",\"%s\",%d", input.c_str(), sparse.c_str(),
                                       IDaxis, min, max, path.c_str(), prefix_out.c_str(), useCache);
        Printf("%s", args.Data());
        userJob.Exec(args.Data());
    }
    return 0;
}
