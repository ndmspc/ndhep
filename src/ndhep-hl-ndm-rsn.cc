#include <getopt.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TH2D.h>
#include <ndm/Config.hh>
#include <ndm/Space.hh>
#include "HighlightNdmRsn.hh"
#include "ndhep.hh"

#define NDHEP_SUBPROG "hl-ndm-rsn"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    spdlog::info("{}-{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    spdlog::info("NDHep ndm highlight {}.", NDHEP_NAME);
    spdlog::info("");
    spdlog::info("Usage: {}-{} [OPTION]...", NDHEP_NAME, NDHEP_SUBPROG);
    spdlog::info("");
    spdlog::info("Options:");
    spdlog::info("       -c, --config[=VALUE]     ndm config file");
    spdlog::info("       -m, --macro[=VALUE]      macro filename");
    spdlog::info("       -a, --prefix[=VALUE]     prefix string");
    spdlog::info("       -z, --postfix[=VALUE]    postfix string");
    spdlog::info("           --silent             no logs printed");
    spdlog::info("           --debug              debug logs printed");
    spdlog::info("           --trace              trace logs printed");
    spdlog::info("");
    spdlog::info("       -h, --help               display this help and exit");
    spdlog::info("       -v, --version            output version information and exit");
    spdlog::info("");
    spdlog::info("Examples:");
    spdlog::info("       ndhep-hl-ndm-rsn -c ndim.yaml");
    spdlog::info("                                Using ndim.yaml config file");
    spdlog::info("");
    spdlog::info("Report bugs to: martin.vala@cern.ch");
    spdlog::info("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    spdlog::info("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{
    // ***** Default values *****
    /// Config file
    std::string configFile;

    std::string macroFileName;
    std::string macroArgs;
    std::string prefix  = "root://eos-hlit.jinr.ru//eos/hybrilit.jinr.ru/projects/lit/hybrilit/ndm/test";
    std::string postfix = "RsnProjection.root";

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("NDHEP_DEBUG_LEVEL")) debugLevel = atoi(getenv("NDHEP_DEBUG_LEVEL"));
    // ***** Default values END *****

    std::string   shortOpts  = "hvc:m:a:z:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"config", required_argument, nullptr, 'c'},
                                {"macro", required_argument, nullptr, 'm'},
                                {"prefix", required_argument, nullptr, 'a'},
                                {"postfix", required_argument, nullptr, 'z'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 'c': configFile = optarg; break;
        case 'm': macroFileName = optarg; break;
        case 'a': prefix = optarg; break;
        case 'z': postfix = optarg; break;
        default: help();
        }
    } while (nextOption != -1);

    spdlog::set_level(static_cast<spdlog::level::level_enum>(debugLevel));

    spdlog::info("{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_VERSION_MAJOR(NDHEP_VERSION), NDHEP_VERSION_MINOR(NDHEP_VERSION),
                 NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);

    if (macroFileName.empty() || configFile.empty()) {
        help();
        return 1;
    }

    TApplication theApp("App", &argc, argv);

    auto mainCanvas = new TCanvas("Main", "Main window", 0, 0, 400, 400);

    NDHep::HighlightNdmRsn h;
    if (h.Load(configFile)) {
        spdlog::error("Problem loading config file '{}' !!! Exiting ...", configFile);
    }

    h.LoadMacro(macroFileName, macroArgs);

    h.SetPrefix(prefix,'/');
    h.SetPostfix(postfix);

    auto mainHistogram = h.GetHistogram();
    mainHistogram->SetStats(0);
    mainHistogram->Draw("colz");
    mainCanvas->Update();
    mainHistogram->SetHighlight();

    mainCanvas->Connect("Highlighted(TVirtualPad*,TObject*,Int_t,Int_t)", "NDHep::HighlightNdmRsn", &h,
                        "HandleHighlight(TVirtualPad*,TObject*,Int_t,Int_t)");

    theApp.Run();
    spdlog::drop_all();

    return 0;
}
