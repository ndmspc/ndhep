#include <getopt.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TH2D.h>
#include <TStyle.h>
#include <ndm/Config.hh>
#include <ndm/Space.hh>
#include "HighlightNdm.hh"
#include "ndhep.hh"

#define NDHEP_SUBPROG "hl-ndm"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    spdlog::info("{}-{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    spdlog::info("NDHep ndm highlight {}.", NDHEP_NAME);
    spdlog::info("");
    spdlog::info("Usage: {}-{} [OPTION]...", NDHEP_NAME, NDHEP_SUBPROG);
    spdlog::info("");
    spdlog::info("Options:");
    spdlog::info("       -c, --config[=VALUE]          ndm config file");
    spdlog::info("       -s, --square-size[=VALUE]     hover square size[VALUExVALUE]");
    spdlog::info("           --silent                  no logs printed");
    spdlog::info("           --debug                   debug logs printed");
    spdlog::info("           --trace                   trace logs printed");
    spdlog::info("");
    spdlog::info("       -h, --help                    display this help and exit");
    spdlog::info("       -v, --version                 output version information and exit");
    spdlog::info("");
    spdlog::info("Examples:");
    spdlog::info("       ndhep-hl-ndm -c ndim.yaml -s 3");
    spdlog::info("                                     Using ndim.yaml config file with hover square 3x3");
    spdlog::info("");
    spdlog::info("Report bugs to: martin.vala@cern.ch");
    spdlog::info("Pkg home page: <https://gitlab.openbrain.sk/salsa/salsa>");
    spdlog::info("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{
    // ***** Default values *****
    /// Config file
    std::string configFile;
    int         squareSize = 1;

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("NDHEP_DEBUG_LEVEL")) debugLevel = atoi(getenv("NDHEP_DEBUG_LEVEL"));
    // ***** Default values END *****

    std::string   shortOpts  = "hvc:s:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"config", required_argument, nullptr, 'c'},
                                {"square-size", required_argument, nullptr, 's'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 'c': configFile = optarg; break;
        case 's': squareSize = atoi(optarg); break;
        default: help();
        }
    } while (nextOption != -1);

    spdlog::set_level(static_cast<spdlog::level::level_enum>(debugLevel));

    spdlog::info("{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_VERSION_MAJOR(NDHEP_VERSION), NDHEP_VERSION_MINOR(NDHEP_VERSION),
                 NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);

    TApplication theApp("App", &argc, argv);

    NDM::Config cfg;
    if (!cfg.load(configFile)) {
        spdlog::critical("Problem loading config file [{}] !!!", configFile);
        return 1;
    }
    NDM::Space * space = cfg.space();
    if (space == nullptr) {
        spdlog::critical("Problem loading Space object from config file [{}] !!!", configFile);
        return 2;
    }
    if (space->axes().size() <= 0) {
        spdlog::error("No axis was found in config file [{}]!", configFile);
        return 3;
    }
    if (squareSize < 1) {
        spdlog::warn("Square-size less than 1! Setting square-size to 1 ...");
        squareSize = 1;
    }

    std::vector<double> min[2];
    for (int iAxis = 0; iAxis < space->axes().size(); iAxis++) {

        space->axes().at(iAxis).split(min[iAxis], cfg.levels()[iAxis]);
        min[iAxis].push_back(space->axes().at(iAxis).max());
    }

    auto  mainCanvas    = new TCanvas("Main", "Main window", 0, 0, 400, 400);
    auto  mainHistogram = new TH2D("n", "Highlight", min[0].size() - 1, &min[0][0], min[1].size() - 1, &min[1][0]);
    Int_t palette[4];
    palette[0] = kWhite;
    palette[1] = kYellow;
    palette[2] = kGreen;
    palette[3] = kRed;
    gStyle->SetPalette(4, palette);
    mainHistogram->SetMinimum(0);
    mainHistogram->SetMaximum(4);
    mainHistogram->SetStats(0);
    mainHistogram->Draw("colz");
    mainCanvas->Update();
    mainHistogram->SetHighlight();
    NDHep::HighlightNdm h(cfg.space(), cfg.levels(), squareSize);
    mainCanvas->Connect("Highlighted(TVirtualPad*,TObject*,Int_t,Int_t)", "NDHep::HighlightNdm", &h,
                        "HandleHighlight(TVirtualPad*,TObject*,Int_t,Int_t)");

    theApp.Run();
    spdlog::drop_all();

    return 0;
}
