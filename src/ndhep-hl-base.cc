#include <TApplication.h>
#include <TCanvas.h>
#include <TH2D.h>
#include <TH2S.h>
#include "HighlightBase.hh"
#include "ndhep.hh"

int main(int argc, char ** argv)
{
    spdlog::info("{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_VERSION_MAJOR(NDHEP_VERSION), NDHEP_VERSION_MINOR(NDHEP_VERSION),
                 NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);

    TApplication theApp("App", &argc, argv);

    auto mainCanvas = new TCanvas("Main", "Main window", 0, 0, 600, 600);
    auto mainHistorgam = new TH2D("n", "Highlight", 10, -5, 5, 10, -5, 5);
    mainHistorgam->SetStats(0);
    mainHistorgam->Draw("colz");
    mainCanvas->Update();
    mainHistorgam->SetHighlight();
    NDHep::HighlightBase h;
    mainCanvas->Connect("Highlighted(TVirtualPad*,TObject*,Int_t,Int_t)", "NDHep::HighlightBase", &h,
                        "HandleHighlight(TVirtualPad*,TObject*,Int_t,Int_t)");

    theApp.Run();
    spdlog::drop_all();

    return 0;
}
