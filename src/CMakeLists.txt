set (MY_INCLUDE_DIRS
	${CMAKE_SOURCE_DIR}/Event
	${CMAKE_SOURCE_DIR}/Highlight
	${SPDLOG_INCLUDE_DIR}
	${YAML_INCLUDE_DIR}
	${PROTOBUF_INCLUDE_DIR}
	${NDM_INCLUDE_DIRS}
)
include_directories(${MY_INCLUDE_DIRS})

set(MY_EXTERNAL_LIBS
	${FMT_LIBRARY}
	ndim
	ndim_proto
	NDHepHighlight
)

file(GLOB EXEC_SRCS RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} "*.cc" )
string(REPLACE ".cc" "" EXECS "${EXEC_SRCS}")
foreach(t ${EXECS})
    RootBin(${t} "${t}.cc" "${MY_EXTERNAL_LIBS}")
endforeach(t)

