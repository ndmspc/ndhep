#pragma once
#include <string>
#include <daos.h>

class TObject;
class TMessage;

namespace NDHep {

///
/// \class DaosKeyValue
///
/// \brief DaosKeyValue object
///	\author Martin Vala <mvala@cern.ch>
///

class DaosKeyValue {

    enum handleType {
        HANDLE_POOL,
        HANDLE_CO,
    };

public:
    DaosKeyValue(std::string pool, std::string container);
    virtual ~DaosKeyValue();

    bool Open();
    bool Close();

    unsigned int Write(std::string key, TObject * o);
    TObject *    Read(std::string key);

protected:
    inline void HandleShare(daos_handle_t * hdl, int type);

private:
    std::string   fPool;          /// UUID of pool (string)
    std::string   fContainer;     /// UUID of container (string)
    uuid_t        fPoolUUID;      /// UUID of pool
    uuid_t        fContainerUUID; /// UUID of container
    daos_handle_t fPoolOH;        /// Pool handle
    daos_handle_t fContainerOH;   /// Container handle
    TMessage *    fMsg = nullptr; // Current message

    bool fIsOpen; /// Flag if container is open
};

} // namespace NDHep