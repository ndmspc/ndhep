#include <TObject.h>
#include <TMessage.h>
#include "ndhep.hh"
#include "DaosKeyValue.hh"

namespace NDHep {

class MyMessage : public TMessage {
public:
    MyMessage(void * buf, Int_t len) : TMessage(buf, len) {}
};

DaosKeyValue::DaosKeyValue(std::string pool, std::string container)
    : fPool(pool), fContainer(container), fIsOpen(kFALSE)
{
    ///
    /// A constructor
    ///
}

DaosKeyValue::~DaosKeyValue()
{
    ///
    /// A destructor
    ///
}

bool DaosKeyValue::Open()
{
    ///
    /// Opens container from pool
    ///
    fIsOpen = false;

    if (uuid_parse(fPool.data(), fPoolUUID)) {
        spdlog::error("[DaosKeyValue::Open] Problem parsing pool uuid '{}' !!!", fPool);
        return false;
    }

    if (uuid_parse(fContainer.data(), fContainerUUID)) {
        spdlog::error("[DaosKeyValue::Open] Problem parsing container uuid '{}' !!!", fContainer);
        return false;
    }

    if (daos_init()) {
        spdlog::error("[DaosKeyValue::Open] Problem in 'daos_init' !!!");
        return false;
    }

    if (daos_pool_connect(fPoolUUID, NULL, DAOS_PC_RW, &fPoolOH, NULL, NULL)) {
        spdlog::error("[DaosKeyValue::Open] Problem in 'daos_pool_connect' for pool : {} !!!", fPool);
        return false;
    }

    HandleShare(&fPoolOH, HANDLE_POOL);

    if (daos_cont_open(fPoolOH, fContainerUUID, DAOS_COO_RW, &fContainerOH, NULL, NULL)) {
        spdlog::error("[DaosKeyValue::Open] Problem in 'daos_cont_open' for pool={} container={} !!!", fPool,
                      fContainer);
        return false;
    }

    fIsOpen = true;
    return true;
}

bool DaosKeyValue::Close()
{
    ///
    /// Closes container from pool
    ///

    HandleShare(&fContainerOH, HANDLE_CO);
    if (daos_cont_close(fContainerOH, NULL)) {
        spdlog::error("[DaosKeyValue::Close] Problem in 'daos_cont_close' for container : {} !!!", fContainer);
        return false;
    }

    if (daos_pool_disconnect(fPoolOH, NULL)) {
        spdlog::error("[DaosKeyValue::Close] Problem in 'daos_pool_disconnect' for pool : {} !!!", fPool);
        return false;
    }

    if (daos_fini()) {
        spdlog::error("[DaosKeyValue::Close] Problem in 'daos_fini' !!!");
        return false;
    }

    return true;
}

inline void DaosKeyValue::HandleShare(daos_handle_t * hdl, int type)
{
    ///
    /// HandleShare
    ///

    d_iov_t ghdl = {NULL, 0, 0};
    int     rc;

    /** fetch size of global handle */
    if (type == HANDLE_POOL)
        rc = daos_pool_local2global(*hdl, &ghdl);
    else
        rc = daos_cont_local2global(*hdl, &ghdl);

    if (rc) {
        spdlog::error("[DaosKeyValue::HandleShare] local2global failed with {}", rc);
        return;
    }

    /** allocate buffer for global pool handle */
    ghdl.iov_buf = malloc(ghdl.iov_buf_len);
    ghdl.iov_len = ghdl.iov_buf_len;

    /** generate actual global handle to share with peer tasks */
    if (type == HANDLE_POOL)
        rc = daos_pool_local2global(*hdl, &ghdl);
    else
        rc = daos_cont_local2global(*hdl, &ghdl);
    if (rc) {
        spdlog::error("[DaosKeyValue::HandleShare] local2global failed with {}", rc);
        return;
    }

    free(ghdl.iov_buf);
}

unsigned int DaosKeyValue::Write(std::string key, TObject * o)
{
    ///
    /// Write object to key
    ///

    if (!fIsOpen) {
        spdlog::error("[DaosKeyValue::Write] Pool:Container '{}:{}' is not open !!!", fPool, fContainer);
        return 0;
    }
    if (o == nullptr) return false;

    daos_handle_t oh;
    daos_obj_id_t oid;
    char          k[32] = {0};
    int           i     = 0;

    oid.hi = 0;
    oid.lo = 4;
    /** the KV API requires the flat feature flag be set in the oid */
    daos_obj_generate_oid(fContainerOH, &oid, DAOS_OF_KV_FLAT, OC_SX, 0, 0);

    if (daos_kv_open(fContainerOH, oid, DAOS_OO_RW, &oh, NULL)) {
        spdlog::error("[DaosKeyValue::Write] Problem in 'daos_kv_open' !!!");
        return 0;
    }

    // sprintf(k, "%s", key.data());
    // sprintf(k, "kkey_%s", key.data());
    // sprintf(k, "key_2");
    // k[5] = '\0';

    snprintf (k, 32, "%.32s", key.data());

    if (fMsg == nullptr) fMsg = new TMessage(kMESS_OBJECT);
    fMsg->Reset();
    fMsg->WriteObject(o);
    unsigned int buf_len = (unsigned int)fMsg->Length();
    char *       buf     = fMsg->Buffer();
    spdlog::debug("[DaosKeyValue::Write] key={} size={}", k, buf_len);

    if (daos_kv_put(oh, DAOS_TX_NONE, 0, k, buf_len, buf, NULL)) {
        spdlog::error("[DaosKeyValue::Write] Problem in 'daos_kv_put' for key={} size={}!!!", k, buf_len);
        return 0;
    }

    spdlog::info("Bytes written: {}", buf_len);

    return buf_len;
}

TObject * DaosKeyValue::Read(std::string key)
{
    ///
    /// Read object from key
    ///

    if (!fIsOpen) {
        spdlog::error("[DaosKeyValue::Read] Pool:Container '{}:{}' is not open !!!", fPool, fContainer);
        return nullptr;
    }

    daos_handle_t oh;
    daos_obj_id_t oid;
    daos_size_t   size;
    char          k[32] = {0};

    oid.hi = 0;
    oid.lo = 4;
    /** the KV API requires the flat feature flag be set in the oid */
    daos_obj_generate_oid(fContainerOH, &oid, DAOS_OF_KV_FLAT, OC_SX, 0, 0);

    if (daos_kv_open(fContainerOH, oid, DAOS_OO_RW, &oh, NULL)) {
        spdlog::error("[DaosKeyValue::Read] Problem in 'daos_kv_open' !!!");
        return nullptr;
    }

    sprintf(k, "%s", key.data());

    if (daos_kv_get(oh, DAOS_TX_NONE, 0, k, &size, NULL, NULL)) {
        spdlog::error("[DaosKeyValue::Read] Problem in 'daos_kv_get' for key={} !!!", key);
        return nullptr;
    }

    char rbuf[size];

    if (daos_kv_get(oh, DAOS_TX_NONE, 0, k, &size, rbuf, NULL)) {
        spdlog::error("[DaosKeyValue::Read] Problem in 'daos_kv_get' for key={} !!!", key);
        return nullptr;
    }

    // memset(rbuf, 0, size);

    MyMessage * m = new MyMessage(rbuf, size);
    // msg->SetBuffer(buf, buf_len); // filling message buffer

    spdlog::info("Read size: {}", size);

    TObject * o = (TObject *)m->ReadObjectAny(m->GetClass());
    if (o == nullptr) {
        spdlog::error("[DaosKeyValue::Read] Problem serializing of object for key={} !!!", key);
        return nullptr;
    }

    return o;
}

} // namespace NDHep