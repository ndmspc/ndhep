#pragma once
#include <TVirtualPad.h>
#include <ndm/Space.hh>
#include <TObject.h>

namespace NDHep {

///
/// \class HighlightNdm
///
/// \brief HighlightNdm object
///	\author Martin Vala <mvala@cern.ch>
///

class HighlightNdm : public TObject {

public:
    HighlightNdm(NDM::Space * s, std::vector<int> l = {1, 1}, int squareSize = 1);
    virtual ~HighlightNdm();

    void HandleHighlight(TVirtualPad *, TObject *, Int_t, Int_t);

private:
    NDM::Space *     mSpace;           ///< Space pointer
    NDM::Point       mPoint;           ///< Current point
    std::vector<int> mLevels;          ///< List of levels
    std::vector<int> mHistCoordinates; /// Current histogram coordinates
    int              mSquareSize;

    /// \cond CLASSIMP
    ClassDef(HighlightNdm, 1);
    /// \endcond
};
} // namespace NDHep