#if defined(__CINT__) || defined(__ROOTCLING__)

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class NDHep::HighlightBase + ;
#pragma link C++ class NDHep::HighlightNdm + ;
#pragma link C++ class NDHep::HighlightNdmRsn + ;
#endif
