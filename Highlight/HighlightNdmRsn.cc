
#include <TH2D.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TDirectory.h>
#include <TPaveText.h>
#include <TStyle.h>
#include <ndm/Space.hh>
#include "ndhep.hh"
#include "HighlightNdmRsn.hh"

/// \cond CLASSIMP
ClassImp(NDHep::HighlightNdmRsn);
/// \endcond

namespace NDHep {

HighlightNdmRsn::HighlightNdmRsn() : TObject()
{
    ///
    /// A constructor
    ///
}

HighlightNdmRsn::~HighlightNdmRsn()
{
    ///
    /// A destructor
    ///
}

int HighlightNdmRsn::Load(std::string configFile)
{
    ///
    /// Load config ndm config
    ///

    if (!mConfig.load(configFile)) {
        spdlog::critical("Problem loading config file [{}] !!!", configFile);
        return 1;
    }
    mSpace = mConfig.space();
    if (mSpace == nullptr) {
        spdlog::critical("Problem loading Space object from config file [{}] !!!", configFile);
        return 2;
    }
    if (mSpace->axes().size() <= 0) {
        spdlog::error("No axis was found in config file [{}]!", configFile);
        return 3;
    }

    mLevels = mConfig.levels();

    mHistCoordinates.resize(mLevels.size());

    return 0;
}

int HighlightNdmRsn::LoadMacro(std::string filename, std::string args)
{
    mMacroArgs = args;
    if (mMacro != nullptr) return 1;
    mMacro = new TMacro(filename.data());
    return 0;
}

TH2S * HighlightNdmRsn::GetHistogram()
{
    ///
    /// Returns histogram
    ///

    std::vector<double> min[2];
    for (int iAxis = 0; iAxis < mSpace->axes().size(); iAxis++) {
        mSpace->axes().at(iAxis).split(min[iAxis], mConfig.levels()[iAxis]);
        min[iAxis].push_back(mSpace->axes().at(iAxis).max());
    }

    mMainHistogram = new TH2S("n", "Highlight", min[0].size() - 1, &min[0][0], min[1].size() - 1, &min[1][0]);
    Int_t palette[4];
    palette[0] = kWhite;
    palette[1] = kYellow;
    palette[2] = kGreen;
    palette[3] = kRed;
    gStyle->SetPalette(4, palette);
    mMainHistogram->SetMaximum(4);

    return mMainHistogram;
}

void HighlightNdmRsn::HandleHighlight(TVirtualPad *, TObject * obj, Int_t x, Int_t y)
{
    ///
    /// Handle highlight
    ///

    if (mSpace == nullptr) {
        spdlog::error("Space is nullptr !!! Skipping ...");
        return;
    }
    if (mMacro == nullptr) {
        spdlog::error("Macro is nullptr !!! Skipping ...");
        return;
    }

    if (!mProjectionCanvas) mProjectionCanvas = new TCanvas("projCanvas", "Rsn Projection", 400, 0, 600, 600);

    auto histo = (TH2D *)obj;
    if (!histo->IsHighlight()) // after highlight is disabled
        spdlog::info("Highlight disabled.");
    else {
        if (histo->GetBinContent(x, y) == 3) {
            mProjectionCanvas->Clear();
            TPaveText pt(.15, .5, .90, .6);
            pt.AddText("No histograms found.");
            pt.Draw();
            mProjectionCanvas->Update();
            return;
        }
        histo->SetBinContent(x, y, (histo->GetBinContent(x, y) + 1));
        mHistCoordinates[0] = x;
        mHistCoordinates[1] = y;
        mSpace->find_point(mHistCoordinates, mLevels, mPoint);
        spdlog::info("Hist bin[{},{}] [<{},{}),<{},{})] path={}", x, y, mPoint.coordinates(0).min(),
                     mPoint.coordinates(0).max(), mPoint.coordinates(1).min(), mPoint.coordinates(1).max(),
                     mPoint.path());

        TDirectory * folder = nullptr;
        if (mUseCache) {
            folder = CreateFolderFromPath(mPoint.path());
        }

        std::string args = fmt::format("\"{}{}{}\",(TCanvas *){},(TDirectory *){}", mPrefix, mPoint.path(), mPostfix,
                                       static_cast<void *>(mProjectionCanvas),static_cast<void *>(folder));
        spdlog::debug("{}", args);

        mProjectionCanvas->Clear();
        Long_t rc = mMacro->Exec(args.data());
        spdlog::debug("macro=[{}] rc=[{}]", mMacro->GetName(), rc);
        if (!rc) {
            histo->SetBinContent(x, y, 2);
            mProjectionCanvas->Update();
        }
        else {
            histo->SetBinContent(x, y, 3); // error
            mProjectionCanvas->Clear("D");
            TPaveText pt(.15, .5, .90, .6);
            pt.Clear();
            pt.AddText("No histograms found.");
            pt.Draw();
            mProjectionCanvas->Update();
        }
    }
}

void HighlightNdmRsn::SetPrefix(std::string s, char c)
{
    ///
    /// Sets prefix
    ///

    if (!s.empty()) {
        if (c)
            if (s.back() != c) s.push_back(c);
    }
    mPrefix = s;
}

TDirectory * HighlightNdmRsn::CreateFolderFromPath(std::string path, TDirectory * root, std::string cacheName,
                                                   std::string cacheTitle)
{
    if (root == nullptr) {
        root = (TDirectory *)gROOT->FindObject(cacheName.data());
        if (root == nullptr) root = gDirectory->mkdir(cacheName.data(), cacheTitle.data());
    }
    TDirectory * folder = root->GetDirectory(path.data());
    if (folder == nullptr) {
        root->mkdir(path.data());
        folder = root->GetDirectory(path.data());
    }
    return folder;
}

} // namespace NDHep