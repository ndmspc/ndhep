#include <TH2D.h>
#include "ndhep.hh"
#include "HighlightNdm.hh"

/// \cond CLASSIMP
ClassImp(NDHep::HighlightNdm);
/// \endcond

namespace NDHep {

HighlightNdm::HighlightNdm(NDM::Space * s, std::vector<int> l, int squareSize)
    : TObject(), mSpace(s), mLevels(l), mSquareSize(squareSize)
{
    ///
    /// A constructor
    ///
    if (!s) return;
    if (squareSize < 0) return;
    //
    // TODO Compare space->axis and mLevels
    //
    mHistCoordinates.resize(l.size());
}

HighlightNdm::~HighlightNdm()
{
    ///
    /// A destructor
    ///
}

void HighlightNdm::HandleHighlight(TVirtualPad * pad, TObject * obj, Int_t x, Int_t y)
{
    ///
    /// Handle highlight
    ///
    spdlog::info("square size=[{}]", mSquareSize);

    if (!mSpace) {
        spdlog::error("Space is nullptr !!! Skipping ...");
        return;
    }
    auto histo = (TH2D *)obj;
    if (!histo->IsHighlight()) // after highlight is disabled
        spdlog::info("Highlight disabled.");
    else {
        spdlog::info("Bin [{},{}]", x, y);
        int lStep, rStep;
        if (mSquareSize % 2 == 0) {
            lStep = floor(mSquareSize / 2) - 1;
            rStep = floor(mSquareSize / 2);
        }
        else {
            lStep = floor(mSquareSize / 2);
            rStep = floor(mSquareSize / 2);
        }

        int xMin = (x - lStep < 1) ? 1 : x - lStep;
        int xMax = (x + rStep > histo->GetXaxis()->GetNbins()) ? histo->GetXaxis()->GetNbins() : x + rStep;
        int yMin = (y - lStep < 1) ? 1 : y - lStep;
        int yMax = (y + rStep > histo->GetYaxis()->GetNbins()) ? histo->GetYaxis()->GetNbins() : y + rStep;
        std::vector<NDM::Point> points;

        for (int iX = xMin; iX <= xMax; iX++) {
            for (int iY = yMin; iY <= yMax; iY++) {

                if (histo->GetBinContent(iX, iY) == 0) {
                    histo->SetBinContent(iX, iY, 1);
                    mHistCoordinates[0] = x;
                    mHistCoordinates[1] = y;
                    NDM::Point p;
                    mSpace->find_point(mHistCoordinates, mLevels, p);
                    points.push_back(p);
                }
            }
        }
        spdlog::info("Vector size of points= {}", points.size());
        pad->Modified();
        pad->Update();
    }
}

} // namespace NDHep