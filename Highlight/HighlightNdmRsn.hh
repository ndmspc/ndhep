#pragma once
#include <TVirtualPad.h>
#include <TH2S.h>
#include <TMacro.h>
#include <ndm/Config.hh>
#include <ndm/Point.pb.h>
#include <TObject.h>
namespace NDHep {

///
/// \class HighlightNdmRsn
///
/// \brief HighlightNdmRsn object
///	\author Martin Vala <mvala@cern.ch>
///

class HighlightNdmRsn : public TObject {

public:
    HighlightNdmRsn();
    virtual ~HighlightNdmRsn();

    int  Load(std::string configFile);
    int  LoadMacro(std::string filename, std::string args = "");
    void SetMacroArgs(std::string args = "") { mMacroArgs = args; }

    void SetPrefix(std::string s, char c = 0);
    void SetPostfix(std::string s) { mPostfix = s; }

    TH2S * GetHistogram();

    void HandleHighlight(TVirtualPad *, TObject *, Int_t, Int_t);

private:
    NDM::Config      mConfig;                    ///< Config
    NDM::Space *     mSpace{nullptr};            ///< Space pointer
    NDM::Point       mPoint{};                   ///< Current point
    std::vector<int> mLevels{};                  ///< List of levels
    std::vector<int> mHistCoordinates{};         ///< Current histogram coordinates
    TH2S *           mMainHistogram{nullptr};    ///< Main histogram
    TCanvas *        mProjectionCanvas{nullptr}; ///< projection canvas
    TMacro *         mMacro{nullptr};            ///< Macro
    std::string      mMacroArgs{};               ///< Macro arguments
    std::string      mPrefix{};                  ///< Prefix (<prefix><path><postfix>)
    std::string      mPostfix{};                 ///< Postfix (<prefix><path><postfix>)
    bool             mUseCache{true};            ///< Flag if to use cache

    TDirectory * CreateFolderFromPath(std::string path, TDirectory * root = nullptr, std::string cacheName = "cache",
                                      std::string cacheTitle = "Cache Directory");

    /// \cond CLASSIMP
    ClassDef(HighlightNdmRsn, 1);
    /// \endcond
};
} // namespace NDHep