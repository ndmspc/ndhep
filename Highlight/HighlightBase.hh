#pragma once
#include <TVirtualPad.h>
#include <TObject.h>
namespace NDHep
{

///
/// \class HighlightBase
///
/// \brief HighlightBase object
///	\author Martin Vala <mvala@cern.ch>
///

class HighlightBase : public TObject
{

public:
  HighlightBase();
  virtual ~HighlightBase();

  void HandleHighlight(TVirtualPad*,TObject*,Int_t,Int_t);
  
private:

  /// \cond CLASSIMP
  ClassDef(HighlightBase, 1);
  /// \endcond
};
} // namespace NDHep