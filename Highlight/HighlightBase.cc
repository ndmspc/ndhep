#include <TH2D.h>
#include "ndhep.hh"
#include "HighlightBase.hh"

/// \cond CLASSIMP
ClassImp(NDHep::HighlightBase);
/// \endcond

namespace NDHep {

HighlightBase::HighlightBase() : TObject()
{
    ///
    /// A constructor
    ///
}

HighlightBase::~HighlightBase()
{
    ///
    /// A destructor
    ///
}

void HighlightBase::HandleHighlight(TVirtualPad *, TObject *obj, Int_t x, Int_t y)
{
    ///
    /// Handle highlight
    ///
    auto histo = (TH2D *)obj;
    if (!histo->IsHighlight()) // after highlight is disabled
        spdlog::info("Highlight disabled.");
    else {
        spdlog::info("Bin [{},{}]", x, y);
    }
}

} // namespace NDHep