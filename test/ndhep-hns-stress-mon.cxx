#include <getopt.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <TString.h>
#include <TFile.h>
#include <TKey.h>
#include <THnSparse.h>
#include <TStopwatch.h>
#include <TGraph.h>
#include <TH2D.h>
#include <TH3D.h>
#include <czmq.h>
#include "ndhep.hh"

#define NDHEP_SUBPROG "ndhep-hns-stress-mon"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    spdlog::info("{}-{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    spdlog::info("NDHep {}.", NDHEP_NAME);
    spdlog::info("");
    spdlog::info("Usage: {}-{} [OPTION]...", NDHEP_NAME, NDHEP_SUBPROG);
    spdlog::info("");
    spdlog::info("Options:");
    // spdlog::info("       -c, --config[=VALUE]          ndm config file");
    // spdlog::info("       -s, --square-size[=VALUE]     hover square size[VALUExVALUE]");
    spdlog::info("           --silent                  no logs printed");
    spdlog::info("           --debug                   debug logs printed");
    spdlog::info("           --trace                   trace logs printed");
    spdlog::info("");
    spdlog::info("       -h, --help                    display this help and exit");
    spdlog::info("       -v, --version                 output version information and exit");
    spdlog::info("");
    spdlog::info("Examples:");
    // spdlog::info("       ndhep-hl-ndm -c ndim.yaml -s 3");
    // spdlog::info("                                     Using ndim.yaml config file with hover square 3x3");
    spdlog::info("");
    spdlog::info("Report bugs to: martin.vala@cern.ch");
    spdlog::info("Pkg home page: <https://gitlab.com/ndmspc/ndhep>");
    spdlog::info("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{

    // ***** Default values *****
    /// Config file
    std::string url     = "@tcp://*:15000";
    std::string sub     = "";
    int         n_files = 1;
    int         timeout = 10000;

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("NDHEP_DEBUG_LEVEL")) debugLevel = atoi(getenv("NDHEP_DEBUG_LEVEL"));
    // ***** Default values END *****

    std::string   shortOpts  = "hvu:s:n:t:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"url", required_argument, nullptr, 'u'},
                                {"subscribe", required_argument, nullptr, 's'},
                                {"n-files", required_argument, nullptr, 'n'},
                                {"timeout", required_argument, nullptr, 't'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 'u': url = optarg; break;
        case 's': sub = optarg; break;
        case 'n': n_files = atoi(optarg); break;
        case 't': timeout = atoi(optarg)*1000; break;
        default: help();
        }
    } while (nextOption != -1);

    spdlog::set_level(static_cast<spdlog::level::level_enum>(debugLevel));

    spdlog::info("{} [{}] v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);

    spdlog::info("url={} sub={}", url, sub);
    zsock_t * socket = zsock_new_sub(url.data(), sub.data());
    assert(socket);
    zmsg_t * msg;
    char *   timeStr, *sizeStr, *fileNameStr;
    double   time;
    int      size;
    double   firstTime, curTime = -1;

    double n_files_id = TMath::Log2(n_files);

    Long64_t     sizeSum = 0;
    TStopwatch * timer   = nullptr;

    TH1::AddDirectory(kFALSE);

    TGraph * grSpeed         = nullptr;
    TH2D *   hTime           = nullptr;
    TH2D *   hW              = nullptr;
    TH2D *   hSpeedAvg       = nullptr;
    TH3D *   hSpeedAvgNFiles = nullptr;

    zpoller_t * poller = zpoller_new(socket, NULL);

    spdlog::info("Reseting timeout : {} s [{} file(s) ({})]", timeout / 1000, n_files, n_files_id);
    int    iReset = 0;
    double speed;
    while (!zctx_interrupted) {
        zsock_t * which = (zsock_t *)zpoller_wait(poller, timeout);
        if (zpoller_expired(poller)) {
            if (sizeSum > 0 || iReset == 0) {
                if (iReset > 0)
                    spdlog::info("Reseting timer due to no activity for more then {} s ...", timeout / 1000);
                delete timer;
                timer   = nullptr;
                sizeSum = 0;
                curTime = -1;

                iReset++;
                if (hTime && iReset > 0) {
                    std::string monfname = fmt::format("mon_{}.root", iReset);
                    spdlog::info("Creating file '{}' ...", monfname);
                    TFile f(monfname.data(), "RECREATE");
                    hTime->SetMaximum(hTime->GetMaximum());
                    hTime->Write();
                    hW->SetMaximum(hW->GetMaximum());
                    hW->Write();
                    hSpeedAvg->SetMaximum(hSpeedAvg->GetMaximum());
                    hSpeedAvg->Write();
                    hSpeedAvgNFiles->SetMaximum(hTime->GetMaximum());
                    hSpeedAvgNFiles->Write();
                    hTime->SetMaximum(hTime->GetMaximum());
                    grSpeed->Write();
                    delete hTime;
                    hTime = nullptr;
                    delete hW;
                    hW = nullptr;
                    delete hSpeedAvg;
                    hSpeedAvg = nullptr;
                    delete hSpeedAvgNFiles;
                    hSpeedAvgNFiles = nullptr;
                    delete grSpeed;
                    grSpeed = nullptr;
                    spdlog::info("Results were written to file '{}' ...", monfname);
                }
            }
            continue;
        }
        if (!which) break;

        if (hTime == nullptr) {
            hTime = new TH2D("hTime", "Time vs type", 100, 0, 100, 2, 0, 2);
            hTime->GetXaxis()->SetTitle("time[s]");
            hTime->GetYaxis()->SetTitle("type");
            hTime->GetYaxis()->SetBinLabel(1, "kv");
            hTime->GetYaxis()->SetBinLabel(2, "posix");
        }
        if (hW == nullptr) {
            hW = new TH2D("hW", "W_OK vs type", 2, 0, 2, 2, 0, 2);
            hW->GetXaxis()->SetTitle("status");
            hW->GetXaxis()->SetBinLabel(1, "fail");
            hW->GetXaxis()->SetBinLabel(2, "ok");
            hW->GetYaxis()->SetTitle("type");
            hW->GetYaxis()->SetBinLabel(1, "kv");
            hW->GetYaxis()->SetBinLabel(2, "posix");
        }
        if (hSpeedAvg == nullptr) {
            hSpeedAvg = new TH2D("hSpeedAvg", "SpeedAvg vs type", 100, 0, 10000, 2, 0, 2);
            hSpeedAvg->GetXaxis()->SetTitle("speed[MB/s]");
            hSpeedAvg->GetYaxis()->SetTitle("type");
            hSpeedAvg->GetYaxis()->SetBinLabel(1, "kv");
            hSpeedAvg->GetYaxis()->SetBinLabel(2, "posix");
        }
        if (hSpeedAvgNFiles == nullptr) {
            hSpeedAvgNFiles = new TH3D("hSpeedAvgNFiles", "Speed Avg vs type vs n", 100, 0, 10000, 2, 0, 2, 10, 0, 10);
            hSpeedAvgNFiles->GetXaxis()->SetTitle("speed[MB/s]");
            hSpeedAvgNFiles->GetYaxis()->SetTitle("type");
            hSpeedAvgNFiles->GetZaxis()->SetTitle("N files");
            hSpeedAvgNFiles->GetYaxis()->SetBinLabel(1, "kv");
            hSpeedAvgNFiles->GetYaxis()->SetBinLabel(2, "posix");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(1, "1");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(2, "2");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(3, "4");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(4, "8");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(5, "16");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(6, "32");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(7, "64");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(8, "128");
            hSpeedAvgNFiles->GetZaxis()->SetBinLabel(9, "256");
        }
        if (grSpeed == nullptr) {
            grSpeed = new TGraph(0);
            grSpeed->SetNameTitle("grSpeed", "Speed");
            grSpeed->GetYaxis()->SetTitle("speed[MB/s]");
            grSpeed->SetMarkerColor(4);
            grSpeed->SetMarkerStyle(21);
        }
        msg = zmsg_recv(which);
        if (!msg) break;

        // zmsg_print(msg);
        timeStr     = zmsg_popstr(msg);
        sizeStr     = zmsg_popstr(msg);
        fileNameStr = zmsg_popstr(msg);
        time        = atof(timeStr);
        size        = atoi(sizeStr);
        sizeSum += size;
        TString fns(fileNameStr);
        if (curTime < 0) curTime = time;

        speed = (double)sizeSum / (1024 * 1024 * curTime);
        if (size == 0) {
            spdlog::error("{} time={:.2f}s size={:.2f}GB sum={:.2f}GB timeSum={:.2f}s ({:.2f} MB/s) [{}]", fileNameStr,
                          time, (double)size / (1024 * 1024 * 1024), (double)sizeSum / (1024 * 1024 * 1024), curTime,
                          speed, n_files_id);
            if (fns.BeginsWith("daos://")) {
                hW->Fill(0., 0);
                hSpeedAvg->Fill(speed, 0);
                hSpeedAvgNFiles->Fill(speed, 0., n_files_id);
                grSpeed->SetPoint(grSpeed->GetN(), grSpeed->GetN(), speed);
            }
            else {
                hW->Fill(0., 1);
                hSpeedAvg->Fill(speed, 1);
                hSpeedAvgNFiles->Fill(speed, 1., n_files_id);
                grSpeed->SetPoint(grSpeed->GetN(), grSpeed->GetN(), speed);
            }
        }
        else {
            if (!timer) {
                timer = new TStopwatch();
                timer->Start();
                curTime = time;
            }
            else {
                timer->Stop();
                curTime += timer->RealTime();
            }

            if (fns.BeginsWith("daos://")) {
                hTime->Fill(time, 0);
                hW->Fill(1., 0);
                hSpeedAvg->Fill(speed, 0);
                hSpeedAvgNFiles->Fill(speed, 0., n_files_id);
                grSpeed->SetPoint(grSpeed->GetN(), grSpeed->GetN(), speed);
            }
            else {
                hTime->Fill(time, 1);
                hW->Fill(1., 1);
                hSpeedAvg->Fill(speed, 1);
                hSpeedAvgNFiles->Fill(speed, 1., n_files_id);
                grSpeed->SetPoint(grSpeed->GetN(), grSpeed->GetN(), speed);
            }

            spdlog::info("{} time={:.2f}s size={:.2f}GB sum={:.2f}GB timeSum={:.2f}s ({:.2f} MB/s) [{}]", fileNameStr,
                         time, (double)size / (1024 * 1024 * 1024), (double)sizeSum / (1024 * 1024 * 1024), curTime,
                         speed, n_files_id);
        }
        timer->Start();

        free(timeStr);
        free(sizeStr);
        free(fileNameStr);
        zmsg_destroy(&msg);
    }

    if (timer) {
        timer->Stop();
        timer->Print();
    }
    // printf("sum=%lld timeSum=%f (%.2f MB)\n", sizeSum, timer->RealTime(), (double)sizeSum / (1024 *
    // 1024)/timer->RealTime());
    zsock_destroy(&socket);

    return 0;
}
