#include <getopt.h>
#include <TArrayI.h>
#include <TAxis.h>
#include <TFile.h>
#include <THnSparse.h>
#include <TList.h>
#include <TMath.h>
#include <TRandom3.h>
#include "ndhep.hh"

#define NDHEP_SUBPROG "ndhep-hns-gen"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    spdlog::info("{}-{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    spdlog::info("NDHep {}.", NDHEP_NAME);
    spdlog::info("");
    spdlog::info("Usage: {}-{} [OPTION]...", NDHEP_NAME, NDHEP_SUBPROG);
    spdlog::info("");
    spdlog::info("Options:");
    // spdlog::info("       -c, --config[=VALUE]          ndm config file");
    // spdlog::info("       -s, --square-size[=VALUE]     hover square size[VALUExVALUE]");
    spdlog::info("           --silent                  no logs printed");
    spdlog::info("           --debug                   debug logs printed");
    spdlog::info("           --trace                   trace logs printed");
    spdlog::info("");
    spdlog::info("       -h, --help                    display this help and exit");
    spdlog::info("       -v, --version                 output version information and exit");
    spdlog::info("");
    spdlog::info("Examples:");
    // spdlog::info("       ndhep-hl-ndm -c ndim.yaml -s 3");
    // spdlog::info("                                     Using ndim.yaml config file with hover square 3x3");
    spdlog::info("");
    spdlog::info("Report bugs to: martin.vala@cern.ch");
    spdlog::info("Pkg home page: <https://gitlab.com/ndmspc/ndhep>");
    spdlog::info("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

TFile *      file    = 0;
THnSparseD * bigHist = 0;
Int_t        iFile   = 0;
std::string  fNamePrefix;
TRandom3     rnd(0);
Long64_t     nFillPerPoint;
Int_t        cCurr       = 0;
Double_t     cMax        = 1;
Int_t        lastPercent = 0;
Bool_t       isDebug     = kFALSE;
Bool_t       isRandom    = kFALSE;

void r(Int_t cDim, Int_t nDim, TArrayD * arr, Double_t * point, Double_t * mean, Double_t * sigma, Int_t * bins,
       Double_t * mins, Double_t * maxs, Long64_t maxMemorySizeMBytes, Int_t maxFiles = kMaxInt)
{
    if (iFile >= maxFiles) return;
    if (cDim == 0) {
        TString fname = TString::Format("%s_%05d.root", fNamePrefix.data(), iFile);
        if (!file) {

            spdlog::info("File: {} maxMemorySize: {}MB", fname.Data(), maxMemorySizeMBytes / (1024 * 1024));
            file    = TFile::Open(fname.Data(), "RECREATE");
            bigHist = new THnSparseD("hBH", "Big Hist", nDim, bins, mins, maxs, 100000000);
        }

        // Filling histogram
        if (isDebug) {
            std::string s;
            for (Int_t i = 0; i < nDim; i++) {
                s = TString::Format("%.2f (%.2f) ", mean[i], sigma[i]).Data();
            }
            spdlog::debug("{}", s);
        }

        spdlog::debug("Filling {}", nFillPerPoint);
        for (Long64_t iFill = 0; iFill < nFillPerPoint; iFill++) {
            for (Int_t i = 0; i < nDim; i++) {
                point[i] = rnd.Gaus(mean[i], sigma[i]);
            }
            bigHist->Fill(point);
        }

        Double_t percent = Double_t(cCurr) / cMax * 100;

        if (lastPercent < Int_t(percent * 10)) {
            printf("\r%02.1f%%", percent);
            fflush(stdout);
            lastPercent = Int_t(percent * 10);
        }
        cCurr++;
        Long64_t nBinsSizeBytes = sizeof(Double_t) * bigHist->GetNbins();
        spdlog::debug("Size {} ({} MB)", nBinsSizeBytes, nBinsSizeBytes / (1024 * 1024));
        if (nBinsSizeBytes >= maxMemorySizeMBytes) {
            // bigHist->Print("all");
            bigHist->Write();
            Printf("");
            Printf("File: %s \n\tSize: mem=%.3f MB file=%.3f MB\n\tnBinsFilled = %"
                   "e(% e) ",
                   fname.Data(), (Double_t)sizeof(Double_t) * bigHist->GetNbins() / (1024 * 1024),
                   (Double_t)file->GetBytesWritten() / (1024 * 1024), (Double_t)bigHist->GetNbins(),
                   bigHist->GetSparseFractionBins());
            file->Close();
            SafeDelete(file);
            iFile++;
        }
    }

    if (cDim > 0) {
        for (Int_t i = 0; i < arr[cDim].GetSize(); i++) {
            mean[cDim] = arr[cDim].At(i);
            r(cDim - 1, nDim, arr, point, mean, sigma, bins, mins, maxs, maxMemorySizeMBytes, maxFiles);
        }
    }
}

void GenArray(TArrayD * arr, Int_t nDim, Bool_t isRandom, Int_t * bins, Double_t * mins, Double_t * maxs)
{
    TRandom3 r(0);
    for (Int_t i = 0; i < nDim; i++) {
        arr[i].Set(i + 1);
        TArrayI indexes(i + 1);
        for (Int_t j = 0; j < i + 1; j++) {
            Double_t v    = j;
            Double_t step = (maxs[i] - mins[i]) / (i + 2);
            if (isRandom) {
                Int_t index = r.Uniform(i + 1);
                for (Int_t k = 0; k < j; k++) {
                    if (indexes.At(k) == index) {
                        k     = 0;
                        index = r.Uniform(i + 1);
                        continue;
                    }
                }
                indexes.AddAt(index, j);
                v = mins[i] + (index + 1) * step;
                arr[i].SetAt(v, j);
            }
            else {
                v = mins[i] + (j + 1) * step;
                arr[i].SetAt(v, j);
            }
        }
    }
}

int main(int argc, char ** argv)
{

    // ***** Default values *****
    /// Config file
    int         nDim                = 10;
    int         nBins               = 20000;
    double      min                 = -nBins / 2;
    double      max                 = nBins / 2;
    Long64_t    nFillPerOnePoint    = 1000;
    Long64_t    maxMemorySizeMBytes = 100;
    std::string fileBaseName        = "/tmp/HnSparse";
    Int_t       maxFiles            = 1;

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("NDHEP_DEBUG_LEVEL")) debugLevel = atoi(getenv("NDHEP_DEBUG_LEVEL"));
    // ***** Default values END *****

    std::string   shortOpts  = "hvn:b:s:m:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"number-per-one-point", required_argument, nullptr, 'n'},
                                {"file-base", required_argument, nullptr, 'b'},
                                {"file-size", required_argument, nullptr, 's'},
                                {"max-files", required_argument, nullptr, 'm'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 'n': nFillPerOnePoint = atoll(optarg); break;
        case 'b': fileBaseName = optarg; break;
        case 's': maxMemorySizeMBytes = atoi(optarg); break;
        case 'm': maxFiles = atoi(optarg); break;
        default: help();
        }
    } while (nextOption != -1);

    spdlog::set_level(static_cast<spdlog::level::level_enum>(debugLevel));

    spdlog::info("{} [{}] v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);

    if (maxFiles < 1) maxFiles = kMaxInt;

    fNamePrefix = fileBaseName;
    maxMemorySizeMBytes *= 1024 * 1024;
    cMax          = TMath::Factorial(nDim);
    nFillPerPoint = nFillPerOnePoint;
    // isDebug       = kTRUE;
    // isRandom      = kTRUE;

    Double_t mean[nDim];
    Double_t sigma[nDim];
    Double_t point[nDim];
    Int_t    bins[nDim];
    Double_t mins[nDim];
    Double_t maxs[nDim];
    for (Int_t i = 0; i < nDim; i++) {
        bins[i]  = nBins;
        mins[i]  = min;
        maxs[i]  = max;
        sigma[i] = (double)nBins / (i + 1);
    }

    TArrayD arr[nDim];
    GenArray(&arr[0], nDim, isRandom, &bins[0], &mins[0], &maxs[0]);

    r(nDim - 1, nDim, &arr[0], &point[0], &mean[0], &sigma[0], &bins[0], &mins[0], &maxs[0], maxMemorySizeMBytes,
      maxFiles);

    if (file && bigHist) {
        Printf("\r100.0%%");
        bigHist->Write();
        TString fname = TString::Format("%s_%05d.root", fileBaseName.data(), iFile);
        Printf("File: %s \n\tSize: mem=%.3f MB file=%.3f MB\n\tnBinsFilled = % "
               "e(% e) ",
               fname.Data(), (Double_t)sizeof(Double_t) * bigHist->GetNbins() / (1024 * 1024),
               (Double_t)file->GetBytesWritten() / (1024 * 1024), (Double_t)bigHist->GetNbins(),
               bigHist->GetSparseFractionBins());
        file->Close();
        SafeDelete(file);
    }

    spdlog::info("Done.");
    return 0;
}
