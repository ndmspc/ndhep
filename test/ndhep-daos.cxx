#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <daos.h>
#include <TH1D.h>
#include <TFile.h>
#include <TMessage.h>
#include <DaosKeyValue.hh>

#include "ndhep.hh"

int main(int argc, char ** argv)
{

    if (argc != 4) {
        fprintf(stderr, "args: pool cont [r|w]\n");
        exit(1);
    }

    // Generate histogram
    TH1D * h;

    std::string p = fmt::format("{}", argv[1]);
    std::string c = fmt::format("{}", argv[2]);
    std::string a = fmt::format("{}", argv[3]);
    std::string k("test");

    if (a == "r") {

        NDHep::DaosKeyValue kv(p, c);
        if (!kv.Open()) {
            spdlog::error("Error in opening pool={} cont={} !!!", p, c);
            return 1;
        }
        h = (TH1D*) kv.Read(k);
        if (h == nullptr) {
            spdlog::error("Error when reading object from key '{}' !!!", k);
        }
        else {
            h->Print();
            TFile *f = TFile::Open("/tmp/daos_kv_test.root","RECREATE");
            if (f) {
                h->Write();
                f->Close();
            }

        }
        kv.Close();
    }
    else if (a == "w") {

        h = new TH1D("h1sl", "h1sl", 100, -3, 3);
        h->FillRandom("gaus", 100000);
        h->Print();

        NDHep::DaosKeyValue kv(p, c);
        if (!kv.Open()) {
            spdlog::error("Error in opening pool={} cont={} !!!", p, c);
            return 1;
        }
        kv.Write(k, h);
        kv.Close();
    }
    else {
        spdlog::error("Error: action can be 'r' or 'w' !!! Current value '{}'", a);
        return 1;
    }

    spdlog::info("ndhep_daos [DONE]");
    return 0;
}
