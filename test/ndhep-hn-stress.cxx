#include <getopt.h>
#include "Histogram.hh"
#include "ndhep.hh"

#define NDHEP_SUBPROG "ndhep-hn-stress"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    spdlog::info("{}-{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    spdlog::info("NDHep {}.", NDHEP_NAME);
    spdlog::info("");
    spdlog::info("Usage: {}-{} [OPTION]...", NDHEP_NAME, NDHEP_SUBPROG);
    spdlog::info("");
    spdlog::info("Options:");
    spdlog::info("       -f, --filename[=VALUE]        name of output file");
    spdlog::info("       -s, --size[=VALUE]            number of fill or size (eg. 100, 100K, 100M, 1G)");
    spdlog::info("           --silent                  no logs printed");
    spdlog::info("           --debug                   debug logs printed");
    spdlog::info("           --trace                   trace logs printed");
    spdlog::info("");
    spdlog::info("       -h, --help                    display this help and exit");
    spdlog::info("       -v, --version                 output version information and exit");
    spdlog::info("");
    spdlog::info("Examples:");
    // spdlog::info("       ndhep-hl-ndm -c ndim.yaml -s 3");
    // spdlog::info("                                     Using ndim.yaml config file with hover square 3x3");
    spdlog::info("");
    spdlog::info("Report bugs to: martin.vala@cern.ch");
    spdlog::info("Pkg home page: <https://gitlab.com/ndmspc/ndhep>");
    spdlog::info("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{

    // ***** Default values *****
    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("NDHEP_DEBUG_LEVEL")) debugLevel = atoi(getenv("NDHEP_DEBUG_LEVEL"));
    std::string sizeStr  = "1e3";
    std::string filename = "/tmp/HnSparse.root";
    // ***** Default values END *****

    std::string   shortOpts  = "hv:f:s:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"filename", required_argument, nullptr, 'f'},
                                {"size", required_argument, nullptr, 's'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 'f': filename = optarg; break;
        case 's': sizeStr = optarg; break;
        default: help();
        }
    } while (nextOption != -1);

    char     sizeChar = sizeStr.back();
    Long64_t size;
    bool     bytes = false;
    if (isalpha(sizeChar)) {
        bytes      = true;
        Long64_t m = 1;
        if (toupper(sizeChar) == 'K') m = 1024;
        if (toupper(sizeChar) == 'M') m = 1024 * 1024;
        if (toupper(sizeChar) == 'G') m = 1024 * 1024 * 1024;

        sizeStr.pop_back();
        size = (Long64_t)atoll(sizeStr.data()) * m;
    }
    else {
        size  = (Long64_t)atoll(sizeStr.data());
        bytes = false;
    }

    spdlog::set_level(static_cast<spdlog::level::level_enum>(debugLevel));

    spdlog::info("{} [{}] v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);

    NDHep::Histogram h;
    h.Stress(size, bytes, filename);
    spdlog::info("Done.");
    return 0;
}
