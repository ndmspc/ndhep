#include <getopt.h>
#include <TString.h>
#include <TFile.h>
#include <TKey.h>
#include <THnSparse.h>
#include <czmq.h>
#include <HnSparse.hh>
#include "ndhep.hh"
#ifdef USE_DAOS
#include <DaosKeyValue.hh>
#endif

#define NDHEP_SUBPROG "ndhep-hns-rw-stress"
[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    spdlog::info("{}-{} v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);
    spdlog::drop_all();
    exit(0);
}
[[noreturn]] void help()
{
    spdlog::set_pattern("%v");
    spdlog::info("NDHep {}.", NDHEP_NAME);
#ifdef USE_DAOS
    spdlog::info("  daos");
#endif
    spdlog::info("");
    spdlog::info("Usage: {}-{} [OPTION]...", NDHEP_NAME, NDHEP_SUBPROG);
    spdlog::info("");
    spdlog::info("Options:");
    // spdlog::info("       -c, --config[=VALUE]          ndm config file");
    // spdlog::info("       -s, --square-size[=VALUE]     hover square size[VALUExVALUE]");
    spdlog::info("           --silent                  no logs printed");
    spdlog::info("           --debug                   debug logs printed");
    spdlog::info("           --trace                   trace logs printed");
    spdlog::info("");
    spdlog::info("       -h, --help                    display this help and exit");
    spdlog::info("       -v, --version                 output version information and exit");
    spdlog::info("");
    spdlog::info("Examples:");
    // spdlog::info("       ndhep-hl-ndm -c ndim.yaml -s 3");
    // spdlog::info("                                     Using ndim.yaml config file with hover square 3x3");
    spdlog::info("");
    spdlog::info("Report bugs to: martin.vala@cern.ch");
    spdlog::info("Pkg home page: <https://gitlab.com/ndmspc/ndhep>");
    spdlog::info("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char ** argv)
{

    // ***** Default values *****
    /// Config file
    int         nFiles         = 1;
    std::string inFileName     = "/tmp/HnSparse_00000.root";
    std::string objName        = "hBH";
    std::string outFileName    = "/tmp/HnSparseWrite.root";
    int         nObjectPerFile = 1;
    std::string key_prefix     = "key";
    std::string pubUrl         = ">tcp://localhost:15000";

    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("NDHEP_DEBUG_LEVEL")) debugLevel = atoi(getenv("NDHEP_DEBUG_LEVEL"));
    // ***** Default values END *****

    std::string   shortOpts  = "hvi:o:n:u:k:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"input", required_argument, nullptr, 'i'},
                                {"output", required_argument, nullptr, 'o'},
                                {"url", required_argument, nullptr, 'u'},
                                {"objects-per-file", required_argument, nullptr, 'n'},
                                {"key-prefix", required_argument, nullptr, 'k'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'h': help();
        case 'v': version(); break;
        case 'i': inFileName = optarg; break;
        case 'o': outFileName = optarg; break;
        case 'n': nObjectPerFile = atoi(optarg); break;
        case 'k': key_prefix = optarg; break;
        case 'u': pubUrl = optarg; break;
        default: help();
        }
    } while (nextOption != -1);

    spdlog::set_level(static_cast<spdlog::level::level_enum>(debugLevel));

    spdlog::info("{} [{}] v{}.{}.{}-{}", NDHEP_NAME, NDHEP_SUBPROG, NDHEP_VERSION_MAJOR(NDHEP_VERSION),
                 NDHEP_VERSION_MINOR(NDHEP_VERSION), NDHEP_VERSION_PATCH(NDHEP_VERSION), NDHEP_VERSION_RELEASE);

    TString     fn = outFileName.data();
    TUrl        u(outFileName.data());
    Bool_t      useDaos = kFALSE;
    std::string pool, cont;
    if (fn.BeginsWith("daos://")) {
        useDaos = kTRUE;
#ifndef USE_DAOS
        spdlog::critical("DAOS is not supported !!! Make sure that 'ndhep' was compiled with daos support !!!");
#endif

        pool = fmt::format("{}", u.GetHost());
        cont = fmt::format("{}", u.GetFile());
        spdlog::debug("daos pool={} cont={}", pool, cont);
    }

    zsock_t * socket = zsock_new_pub(pubUrl.data());
    assert(socket);

    TFile *                     fIn = TFile::Open(inFileName.data());
    NDHep::HnSparseT<TArrayD> * h   = (NDHep::HnSparseT<TArrayD> *)fIn->Get(objName.data());

    TKey * k = (TKey *)fIn->GetListOfKeys()->At(0);
    Printf("%d %.2f MB", k->GetObjlen(), (Double_t)k->GetObjlen() / (1024 * 1024));

    // kv.Write(k, h);
    // kv.Close();
#ifdef USE_DAOS
    NDHep::DaosKeyValue * kv = nullptr;
#endif
    std::string kv_str;
    TFile *     fOut = nullptr;
    Int_t       b;
    for (int i = 0; i < nFiles; ++i) {
        fn = outFileName.data();
        if (nFiles > 1) {
            fn.ReplaceAll(".root", "");
            fn.Append(TString::Format("_%d.root", i).Data());
        }
        Printf("Opening file '%s' ...", fn.Data());
        if (useDaos) {
#ifdef USE_DAOS
            kv = new NDHep::DaosKeyValue(pool, cont);
            if (!kv->Open()) {
                spdlog::error("Error in opening pool={} cont={} !!!", pool, cont);
                return 1;
            }
#endif
        }
        else {
            fOut = TFile::Open(fn.Data(), "RECREATE");
            if (!fOut) {
                spdlog::error("Problem opening file '{}' !!!!", fn);
                break;
            }
            // fOut->SetCompressionSettings(ROOT::RCompressionSetting::EDefaults::kUseAnalysis);
            fOut->SetCompressionLevel(ROOT::RCompressionSetting::ELevel::kUncompressed);
        }
        for (int j = 0; j < nObjectPerFile; ++j) {
            TStopwatch timer;
            timer.Start();
            Printf("Write j=%d", j);
            if (useDaos) {
#ifdef USE_DAOS
                // std::string kv_str = TString::Format("%07d", j).Data();
                // kv_str = fmt::format("txxxxxx{}", j);
                // kv_str = "t12";
                // kv_str = TString::Format("test", j).Data();
                // kv_str = "xxxxx";
                // char kk[4];
                // sprintf(kk, "%d", j);
                // kv_str = kk;
                kv_str = fmt::format("{}_{}", key_prefix, j);
                spdlog::debug("Writing key={}", kv_str);
                b = kv->Write(kv_str, h);
#endif
            }
            else {

                b = h->Write(TString::Format("t%02d", j).Data());
            }
            Printf("Written %d %.2f MB", b, (Double_t)b / (1024 * 1024));
            std::string fn_str = fn.Data();
            if (useDaos) fn_str = fmt::format("{}#{}", fn.Data(), kv_str);
            timer.Stop();
            zmsg_t * msg = zmsg_new();
            zmsg_pushstrf(msg, "%s", fn_str.data());
            zmsg_pushstrf(msg, "%d", b);
            zmsg_pushstrf(msg, "%f", timer.RealTime());
            zmsg_send(&msg, socket);
        }

        if (useDaos) {
#ifdef USE_DAOS
            kv->Close();
#endif
        }
        else {
            fOut->Close();
        }
    }
    fIn->Close();

    zsock_destroy(&socket);

    return 0;
}
