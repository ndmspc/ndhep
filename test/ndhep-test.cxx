#include <Event.hh>
int main(int /*argc*/, char const ** /*argv*/)
{
    NDHep::Event e(0);
    e.BuildVertexRandom();
    NDHep::Track * t = e.AddTrack();
    t->BuildRandom();
    t = e.AddTrack();
    t->BuildRandom();
    t = e.AddTrack();
    t->BuildRandom();
    e.Print();
    return 0;
}
